package josiassena.humbug.messages.presenter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.Telephony;
import android.support.annotation.NonNull;
import android.util.Log;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.io.InvalidObjectException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;

import josiassena.humbug.helpers.Utils;
import josiassena.humbug.helpers.callbacks.NewMessageReceivedListener;
import josiassena.humbug.messages.view.TextMessagesView;
import josiassena.humbug.msg.Conversation;
import josiassena.humbug.msg.Message;
import josiassena.humbug.msg.MessageType;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func0;
import rx.schedulers.Schedulers;

/**
 * File created by josias on 12/30/15.
 */
@SuppressLint ("LongLogTag")
public class TextMessagesPresenterImpl extends MvpBasePresenter<TextMessagesView>
        implements TextMessagesPresenter {

    private static final String TAG = "TextMessagesPresenterImpl";

    @Override
    public void getTextMessages(Context context,
                                final NewMessageReceivedListener newMessageReceivedListener,
                                Conversation conversation) {
        Log.d(TAG, "getTextMessages() called with: conversation = [" + conversation + "]");

        final ArrayList<Message> messages = new ArrayList<>();
        Observable<ArrayList<Message>> smsObservable = getSmsObservable(context, conversation);
//        Observable<ArrayList<Message>> mmsObservable = getMmsObservable(context, conversation);

        smsObservable
                // .mergeWith(mmsObservable) // TODO: 1/22/16 enable when MMS has been implemented
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ArrayList<Message>>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "onCompleted() called with messages.size(): " + messages.size());
                        newMessageReceivedListener.onNewMessagesReceived(messages);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError() called with: " +
                                "message = [" + e.getMessage() + "]", e);
                    }

                    @Override
                    public void onNext(ArrayList<Message> messageArrayList) {
                        messages.addAll(messageArrayList);
                    }
                });
    }

    @Override
    public void getTextMessages(Context context,
                                final NewMessageReceivedListener newMessageReceivedListener,
                                String phoneNumber) {
        Log.d(TAG, "getTextMessages() called with: phoneNumber = [" + phoneNumber + "]");

        final ArrayList<Message> messages = new ArrayList<>();
        Observable<ArrayList<Message>> smsObservable = getSmsObservable(context, phoneNumber);
        Observable<ArrayList<Message>> mmsObservable = getMmsObservable(context, phoneNumber);

        smsObservable
                .mergeWith(mmsObservable)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ArrayList<Message>>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "onCompleted() called with messages.size(): " + messages.size());
                        newMessageReceivedListener.onNewMessagesReceived(messages);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError() called with: " + "message = [" + e.getMessage() + "]",
                                e);
                    }

                    @Override
                    public void onNext(ArrayList<Message> messageArrayList) {
                        messages.addAll(messageArrayList);
                    }
                });
    }

    @Override
    public void openCameraToTakePicture(Activity activity, int requestCode) {
        activity.startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE), requestCode);
    }

    @Override
    public void openCameraToRecordVideo(Activity activity, int requestCode) {
        activity.startActivityForResult(new Intent(MediaStore.INTENT_ACTION_VIDEO_CAMERA),
                requestCode);
    }

    @Override
    public void openPictureGallery(Activity activity, int requestCode) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        activity.startActivityForResult(Intent.createChooser(intent, "Select Picture"),
                requestCode);
    }

    @Override
    public void openVideoGallery(Activity activity, int requestCode) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("video/*");
        activity.startActivityForResult(intent, requestCode);
    }

    @Override
    public void sendFakeMessage(Context context, Message message) {
        Log.d(TAG,
                "sendFakeMessage() called with: " + "context = [" + context + "], simpleSms = [" +
                        message + "]");
        ContentValues contentValues = new ContentValues();
        contentValues.put(Telephony.Sms.ADDRESS, message.getPhoneNumber());
        contentValues.put(Telephony.Sms.BODY, message.getMessage());
        contentValues.put(Telephony.Sms.TYPE, message.getMessageType().getValue());
        contentValues.put(Telephony.Sms.DATE, message.getTimeOfText());
        contentValues.put(Telephony.Sms.DATE_SENT, message.getTimeOfText());
        contentValues.put(Telephony.Sms.LOCKED, message.isFake());

        Log.d(TAG, "sendFakeMessage: " + contentValues.toString());
        context.getContentResolver().insert(Telephony.Sms.Sent.CONTENT_URI, contentValues);
    }

    @Override
    public void sendMessage(Context context, Message message) {
        Utils.sendRealSms(context, message.getMessage(), message.getPhoneNumber(),
                message.getTimeOfText());
    }

    @NonNull
    private Func0<Observable<Long>> getTimeOfOutboxTextObservable(final Context context,
                                                                  final int id) {
        return new Func0<Observable<Long>>() {
            @Override
            public Observable<Long> call() {
                return Observable.just(getTimeOfOutboxText(context, id));
            }
        };
    }

    private long getTimeOfOutboxText(Context context, int id) {
        Log.d(TAG, "getTimeOfOutboxText() called with: id = [" + id + "]");

        Cursor cursor = context.getContentResolver()
                .query(Telephony.Sms.Sent.CONTENT_URI, null, "_id=" + id, null, null);

        long timeOfText = 0;

        if (cursor != null && cursor.moveToFirst()) {
            String date = cursor.getString(cursor.getColumnIndex(Telephony.Sms.Sent.DATE));
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(Long.parseLong(date));

            timeOfText = calendar.getTime().getTime();
            cursor.close();
        }

        Log.d(TAG, "getTimeOfOutboxText() returned: " + timeOfText);
        return timeOfText;
    }

    @NonNull
    private Func0<Observable<Long>> getTimeOfInboxTextObservable(final Context context,
                                                                 final int id) {
        return new Func0<Observable<Long>>() {
            @Override
            public Observable<Long> call() {
                return Observable.just(getTimeOfInboxText(context, id));
            }
        };
    }

    private long getTimeOfInboxText(Context context, int id) {
        Log.d(TAG, "getTimeOfInboxText() called with: id = [" + id + "]");

        Cursor cursor = context.getContentResolver()
                .query(Telephony.Sms.Inbox.CONTENT_URI, null, "_id=" + id, null, null);

        long timeOfText = 0;
        if (cursor != null && cursor.moveToFirst()) {
            String date = cursor.getString(cursor.getColumnIndex(Telephony.Sms.DATE));
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(Long.parseLong(date));

            timeOfText = calendar.getTime().getTime();
            cursor.close();
        }

        Log.d(TAG, "getTimeOfInboxText() returned: " + timeOfText);
        return timeOfText;
    }

    @Override
    public void sendMMS(Context context, String phoneNumber) {
        // TODO: 1/3/16
    }

    private ArrayList<Message> getAllMmsForCurrentConversation(Context context,
                                                               Conversation conversation) {
        Log.d(TAG, "getAllMmsForCurrentConversation() called with: " + "context = [" + context +
                "], conversation = [" + conversation + "]");

        ArrayList<Message> messageArrayList = new ArrayList<>();
        messageArrayList.addAll(getInboxMms(context, conversation));
        messageArrayList.addAll(getSentMms(context, conversation));

        Log.d(TAG, "getAllMmsForCurrentConversation() returned: " + messageArrayList.size());
        return messageArrayList;
    }

    private ArrayList<Message> getInboxMms(Context context, Conversation conversation) {
        Log.d(TAG, "getInboxMms() called with: " + "context = [" + context + "], conversation = [" +
                conversation + "]");

        ArrayList<Message> messageArrayList = new ArrayList<>();
        String selection = "thread_id like '%" + conversation.getId() + "%'";
        Cursor cursor = context.getContentResolver()
                .query(Telephony.Mms.Inbox.CONTENT_URI, null, selection, null, null);

        Utils.logCursorColumns(TAG + ".getInboxMms", cursor);

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                String mmsId = cursor.getString(cursor.getColumnIndex(Telephony.Mms._ID));

                final Message message = new Message();
                message.setIsMms(true);
                message.setName(conversation.getTitle());

                Observable.from(new Bitmap[] {
                        Utils.getMmsImage(context, getMmsPartId(context, mmsId))})
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<Bitmap>() {
                            @Override
                            public void onCompleted() {
                                Log.d(TAG, "getInboxMms.onCompleted() called");
                            }

                            @Override
                            public void onError(Throwable e) {
                                Log.e(TAG, "getInboxMms.message = " + e.getMessage(), e);
                            }

                            @Override
                            public void onNext(Bitmap bitmap) {
                                Log.d(TAG, "getInboxMms.onNext() called with: " + "bitmap = [" +
                                        bitmap + "]");
                                message.setImage(bitmap);
                            }
                        });

                message.setPhoneNumber(getMmsPhoneNumber(context, mmsId));

                String locked = cursor.getString(cursor.getColumnIndex(Telephony.Mms.LOCKED));
                message.setFake(locked.equals("1"));

                message.setMessageType(MessageType.THEM);
                message.setTimeOfText(getTimeOfInboxMms(context,
                        cursor.getInt(cursor.getColumnIndex(Telephony.Mms._ID))));

                messageArrayList.add(message);
                cursor.moveToNext();
            }

            cursor.close();
        }

        return messageArrayList;
    }

    private ArrayList<Message> getSentMms(Context context, Conversation conversation) {
        Log.d(TAG, "getSentMms() called with: " + "context = [" + context + "], conversation = [" +
                conversation + "]");

        ArrayList<Message> messageArrayList = new ArrayList<>();
        String selection = "thread_id like '%" + conversation.getId() + "%'";
        Cursor cursor = context.getContentResolver()
                .query(Telephony.Mms.Sent.CONTENT_URI, null, selection, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            // Utils.logCursorColumns(TAG, cursor);

            while (!cursor.isAfterLast()) {
                String mmsId = cursor.getString(cursor.getColumnIndex(Telephony.Mms._ID));

                final Message message = new Message();
                message.setIsMms(true);
                message.setName(conversation.getTitle());
                message.setPhoneNumber(getMmsPhoneNumber(context, mmsId));

                Observable.from(new Bitmap[] {
                        Utils.getMmsImage(context, getMmsPartId(context, mmsId))})
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<Bitmap>() {
                            @Override
                            public void onCompleted() {
                                Log.d(TAG, "getSentMms.onCompleted() called");
                            }

                            @Override
                            public void onError(Throwable e) {
                                Log.e(TAG, "getSentMms.message = " + e.getMessage(), e);
                            }

                            @Override
                            public void onNext(Bitmap bitmap) {
                                Log.d(TAG, "getSentMms.onNext() called with: " + "bitmap = [" +
                                        bitmap + "]");
                                message.setImage(bitmap);
                            }
                        });

                String locked = cursor.getString(cursor.getColumnIndex(Telephony.Mms.LOCKED));
                message.setFake(locked.equals("1"));

                message.setMessageType(MessageType.US);
                message.setTimeOfText(getTimeOfSentMms(context,
                        cursor.getInt(cursor.getColumnIndex(Telephony.Mms._ID))));

                messageArrayList.add(message);
                cursor.moveToNext();
            }

            cursor.close();
        }

        return messageArrayList;
    }

    private String getMmsPartId(Context context, String mmsId) {
        Log.d(TAG,
                "getMmsPartId() called with: " + "context = [" + context + "], mmsId = [" + mmsId +
                        "]");

        String partId = "";
        String selectionPart = "mid=" + mmsId;
        Uri uri = Uri.parse("content://mms/part");
        Cursor cursor = context.getContentResolver().query(uri, null, selectionPart, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            // Utils.logCursorColumns(TAG + ".getMmsPartId()", cursor);

            while (!cursor.isAfterLast()) {
                partId = cursor.getString(cursor.getColumnIndex(Telephony.Mms.Part._ID));
                cursor.moveToNext();
            }

            cursor.close();
        }

        return partId;
    }

    private ArrayList<Message> getAllSmsForCurrentConversation(final Context context,
                                                               Conversation conversation) {
        Log.d(TAG,
                "getAllSmsForCurrentConversation() called with:  conversation = [" + conversation +
                        "]");

        ArrayList<Message> messageArrayList = new ArrayList<>();
        String selection = "address like '%" + conversation.getPhoneNumber() + "%'";

        final Cursor cursor = context.getContentResolver()
                .query(Telephony.Sms.CONTENT_URI, null, selection, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            // Utils.logCursorColumns(TAG, cursor);

            while (!cursor.isAfterLast()) {
                final Message message = new Message();
                message.setIsMms(false);
                message.setName(conversation.getTitle());
                message.setPhoneNumber(
                        cursor.getString(cursor.getColumnIndex(Telephony.Sms.ADDRESS)));
                message.setMessage(cursor.getString(cursor.getColumnIndex(Telephony.Sms.BODY)));

                String locked = cursor.getString(cursor.getColumnIndex(Telephony.Sms.LOCKED));
                message.setFake(locked.equals("1"));

                try {
                    final MessageType type = MessageType.getMessageType(
                            cursor.getInt(cursor.getColumnIndex(Telephony.Sms.TYPE)));

                    message.setMessageType(type);

                    if (type == MessageType.THEM) {
                        Observable.defer(getTimeOfInboxTextObservable(context,
                                cursor.getInt(cursor.getColumnIndex(Telephony.Sms._ID))))
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new Observer<Long>() {
                                    @Override
                                    public void onCompleted() {

                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        Log.e(TAG, "onError() called with: " +
                                                "e = [" + e.getMessage() + "]", e);
                                    }

                                    @Override
                                    public void onNext(Long aLong) {
                                        if (aLong != null) {
                                            message.setTimeOfText(aLong);
                                        } else {
                                            message.setTimeOfText(0);
                                        }
                                    }
                                });
                    } else {
                        Observable.defer(getTimeOfOutboxTextObservable(context,
                                cursor.getInt(cursor.getColumnIndex(Telephony.Sms._ID))))
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new Observer<Long>() {
                                    @Override
                                    public void onCompleted() {

                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        Log.e(TAG, "onError() called with: " +
                                                "e = [" + e.getMessage() + "]", e);
                                    }

                                    @Override
                                    public void onNext(Long aLong) {
                                        if (aLong != null) {
                                            message.setTimeOfText(aLong);
                                        } else {
                                            message.setTimeOfText(0);
                                        }
                                    }
                                });
                    }
                } catch (InvalidObjectException e) {
                    e.printStackTrace();
                }

                messageArrayList.add(message);
                cursor.moveToNext();
            }

            cursor.close();
        }

        return messageArrayList;
    }

    private Observable<ArrayList<Message>> getMmsObservable(final Context context,
                                                            final Conversation conversation) {
        return Observable.defer(new Func0<Observable<ArrayList<Message>>>() {
            @Override
            public Observable<ArrayList<Message>> call() {
                return Observable.just(getAllMmsForCurrentConversation(context, conversation));
            }
        });
    }

    private Observable<ArrayList<Message>> getSmsObservable(final Context context,
                                                            final Conversation conversation) {
        return Observable.defer(new Func0<Observable<ArrayList<Message>>>() {
            @Override
            public Observable<ArrayList<Message>> call() {
                return Observable.just(getAllSmsForCurrentConversation(context, conversation));
            }
        });
    }

    private Observable<ArrayList<Message>> getMmsObservable(final Context context,
                                                            final String phoneNumber) {
        return Observable.defer(new Func0<Observable<ArrayList<Message>>>() {
            @Override
            public Observable<ArrayList<Message>> call() {
                return Observable.just(getAllMmsForPhoneNumber(context, phoneNumber));
            }
        });
    }

    private Observable<ArrayList<Message>> getSmsObservable(final Context context,
                                                            final String phoneNumber) {
        return Observable.defer(new Func0<Observable<ArrayList<Message>>>() {
            @Override
            public Observable<ArrayList<Message>> call() {
                return Observable.just(getAllSmsForPhoneNumber(context, phoneNumber));
            }
        });
    }

    private ArrayList<Message> getAllMmsForPhoneNumber(Context context, String phoneNumber) {
        Log.d(TAG, "getAllMmsForPhoneNumber() called with: " + "context = [" + context +
                "], phoneNumber = [" + phoneNumber + "]");

        ArrayList<Message> messageArrayList = new ArrayList<>();
        messageArrayList.addAll(getInboxMmsByPhoneNumber(context, phoneNumber));
        messageArrayList.addAll(getSentMmsByPhoneNumber(context, phoneNumber));

        Log.d(TAG, "getAllMmsForPhoneNumber() returned messageArrayList.size(): " +
                messageArrayList.size());
        return messageArrayList;
    }

    private ArrayList<Message> getInboxMmsByPhoneNumber(Context context, String phoneNumber) {
        Log.e(TAG, "getInboxMmsByPhoneNumber() called with: " + "context = [" + context +
                "], phoneNumber = [" + phoneNumber + "]");

        ArrayList<Message> messageArrayList = new ArrayList<>();
        String selection = "address like '%" + phoneNumber + "%'";
        Cursor cursor = context.getContentResolver()
                .query(Telephony.Mms.Inbox.CONTENT_URI, null, selection, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                String mmsId = cursor.getString(cursor.getColumnIndex(Telephony.Mms._ID));

                final Message message = new Message();
                message.setIsMms(true);
                message.setName(Utils.getContactName(context, phoneNumber));

                Observable.from(new Bitmap[] {
                        Utils.getMmsImage(context, getMmsPartId(context, mmsId))})
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<Bitmap>() {
                            @Override
                            public void onCompleted() {
                                Log.d(TAG, "getInboxMms.onCompleted() called");
                            }

                            @Override
                            public void onError(Throwable e) {
                                Log.e(TAG, "getInboxMms.message = " + e.getMessage(), e);
                            }

                            @Override
                            public void onNext(Bitmap bitmap) {
                                Log.d(TAG, "getInboxMms.onNext() called with: " + "bitmap = [" +
                                        bitmap + "]");
                                message.setImage(bitmap);
                            }
                        });

                message.setPhoneNumber(getMmsPhoneNumber(context, mmsId));

                String locked = cursor.getString(cursor.getColumnIndex(Telephony.Mms.LOCKED));
                message.setFake(locked.equals("1"));

                message.setMessageType(MessageType.THEM);
                message.setTimeOfText(getTimeOfInboxMms(context,
                        cursor.getInt(cursor.getColumnIndex(Telephony.Mms._ID))));

                messageArrayList.add(message);
                cursor.moveToNext();
            }

            cursor.close();
        }

        return messageArrayList;
    }

    private ArrayList<Message> getSentMmsByPhoneNumber(Context context, String phoneNumber) {
        Log.d(TAG, "getSentMmsByPhoneNumber() called with: " + "context = [" + context +
                "], phoneNumber = [" + phoneNumber + "]");

        ArrayList<Message> messageArrayList = new ArrayList<>();
        String selection = "address like '%" + phoneNumber + "%'";
        Cursor cursor = context.getContentResolver()
                .query(Telephony.Mms.Sent.CONTENT_URI, null, selection, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                String mmsId = cursor.getString(cursor.getColumnIndex(Telephony.Mms._ID));

                final Message message = new Message();
                message.setIsMms(true);
                message.setName(Utils.getContactName(context, phoneNumber));
                message.setPhoneNumber(getMmsPhoneNumber(context, mmsId));

                Observable.from(new Bitmap[] {
                        Utils.getMmsImage(context, getMmsPartId(context, mmsId))})
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<Bitmap>() {
                            @Override
                            public void onCompleted() {
                                Log.d(TAG, "getSentMms.onCompleted() called");
                            }

                            @Override
                            public void onError(Throwable e) {
                                Log.e(TAG, "getSentMms.message = " + e.getMessage(), e);
                            }

                            @Override
                            public void onNext(Bitmap bitmap) {
                                Log.d(TAG, "getSentMms.onNext() called with: " + "bitmap = [" +
                                        bitmap + "]");
                                message.setImage(bitmap);
                            }
                        });

                String locked = cursor.getString(cursor.getColumnIndex(Telephony.Mms.LOCKED));
                message.setFake(locked.equals("1"));

                message.setMessageType(MessageType.US);
                message.setTimeOfText(getTimeOfSentMms(context,
                        cursor.getInt(cursor.getColumnIndex(Telephony.Mms._ID))));

                messageArrayList.add(message);
                cursor.moveToNext();
            }

            cursor.close();
        }

        return messageArrayList;
    }

    private ArrayList<Message> getAllSmsForPhoneNumber(Context context, String phoneNumber) {
        Log.d(TAG, "getAllSmsForPhoneNumber() called with: " + "context = [" + context +
                "], phoneNumber = [" + phoneNumber + "]");

        ArrayList<Message> messageArrayList = new ArrayList<>();
        String selection = "address like '%" + phoneNumber + "%'";
        Cursor cursor = context.getContentResolver()
                .query(Telephony.Sms.CONTENT_URI, null, selection, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                Message message = new Message();
                message.setIsMms(false);
                message.setName(Utils.getContactName(context, phoneNumber));
                message.setPhoneNumber(
                        cursor.getString(cursor.getColumnIndex(Telephony.Sms.ADDRESS)));
                message.setMessage(cursor.getString(cursor.getColumnIndex(Telephony.Sms.BODY)));

                String locked = cursor.getString(cursor.getColumnIndex(Telephony.Sms.LOCKED));
                message.setFake(locked.equals("1"));

                try {
                    final MessageType type = MessageType.getMessageType(
                            cursor.getInt(cursor.getColumnIndex(Telephony.Sms.TYPE)));

                    message.setMessageType(type);
                    if (type == MessageType.THEM) {
                        message.setTimeOfText(getTimeOfInboxText(context,
                                cursor.getInt(cursor.getColumnIndex(Telephony.Sms._ID))));
                    } else {
                        message.setTimeOfText(getTimeOfOutboxText(context,
                                cursor.getInt(cursor.getColumnIndex(Telephony.Sms._ID))));
                    }

                } catch (InvalidObjectException e) {
                    e.printStackTrace();
                }

                messageArrayList.add(message);
                cursor.moveToNext();
            }

            cursor.close();
        }

        return messageArrayList;
    }

    private String getMmsPhoneNumber(Context context, String id) {
        Log.d(TAG,
                "getMmsPhoneNumber() called with: " + "context = [" + context + "], id = [" + id +
                        "]");

        String selectionAdd = "msg_id=" + id;
        String uriStr = MessageFormat.format("content://mms/{0}/addr", id);
        Uri uriAddress = Uri.parse(uriStr);
        Cursor cursor = context.getContentResolver()
                .query(uriAddress, null, selectionAdd, null, null);
        String phoneNumber = null;

        if (cursor != null && cursor.moveToFirst()) {
            do {
                String number = cursor.getString(cursor.getColumnIndex(Telephony.Mms.Addr.ADDRESS));
                if (number != null) {
                    try {
                        phoneNumber = number.replace("-", "");
                    } catch (NumberFormatException nfe) {
                        if (phoneNumber == null) {
                            phoneNumber = number;
                        }
                    }
                }
            } while (cursor.moveToNext());

            cursor.close();
        }

        return phoneNumber;
    }

    private long getTimeOfInboxMms(Context context, int id) {
        Log.d(TAG,
                "getTimeOfInboxMms() called with: " + "context = [" + context + "], id = [" + id +
                        "]");

        Cursor cursor = context.getContentResolver()
                .query(Telephony.Mms.Inbox.CONTENT_URI, null, "_id=" + id, null, null);

        long timeOfText = 0;
        if (cursor != null && cursor.moveToFirst()) {
            String date = cursor.getString(cursor.getColumnIndex(Telephony.Mms.DATE));
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(Long.parseLong(date));
            timeOfText = calendar.getTime().getTime();
            cursor.close();
        }

        return timeOfText;
    }

    private long getTimeOfSentMms(Context context, int id) {
        Log.d(TAG, "getTimeOfSentMms() called with: " + "context = [" + context + "], id = [" + id +
                "]");
        Cursor cursor = context.getContentResolver()
                .query(Telephony.Mms.Sent.CONTENT_URI, null, "_id=" + id, null, null);

        long timeOfText = 0;
        if (cursor != null && cursor.moveToFirst()) {
            long date = cursor.getLong(cursor.getColumnIndex(Telephony.Mms.Sent.DATE_SENT));
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(date);

            timeOfText = calendar.getTime().getTime();
            cursor.close();
        }

        return timeOfText;
    }

}