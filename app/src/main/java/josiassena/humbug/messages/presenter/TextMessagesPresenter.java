package josiassena.humbug.messages.presenter;

import android.app.Activity;
import android.content.Context;

import com.hannesdorfmann.mosby.mvp.MvpPresenter;

import josiassena.humbug.helpers.callbacks.NewMessageReceivedListener;
import josiassena.humbug.msg.Conversation;
import josiassena.humbug.msg.Message;
import josiassena.humbug.messages.view.TextMessagesView;

/**
 * File created by josias on 12/30/15.
 */
public interface TextMessagesPresenter extends MvpPresenter<TextMessagesView> {

    void getTextMessages(Context context, NewMessageReceivedListener newMessageReceivedListener, Conversation conversation);

    void getTextMessages(Context context, NewMessageReceivedListener newMessageReceivedListener, String phoneNumber);

    void openCameraToTakePicture(Activity activity, int requestCode);

    void openCameraToRecordVideo(Activity activity, int requestCode);

    void openPictureGallery(Activity activity, int requestCode);

    void openVideoGallery(Activity activity, int requestCode);

    void sendFakeMessage(Context context, Message message);

    void sendMessage(Context context, Message message);

    void sendMMS(Context context, String phoneNumber);
}