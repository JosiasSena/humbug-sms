package josiassena.humbug.messages.view.rec_view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import josiassena.humbug.R;

/**
 * File created by josias on 12/30/15.
 */
public class ThemViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.messageTv)
    TextView messageTv;

    @Bind(R.id.timeSentReceived)
    TextView timeSentReceived;

    @Bind(R.id.messagePhoto)
    ImageView messagePhoto;

    public ThemViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
