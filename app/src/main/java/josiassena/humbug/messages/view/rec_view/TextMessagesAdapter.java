package josiassena.humbug.messages.view.rec_view;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.Telephony;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.cocosw.bottomsheet.BottomSheet;

import org.ocpsoft.prettytime.PrettyTime;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import josiassena.humbug.ImageViewActivity;
import josiassena.humbug.R;
import josiassena.humbug.helpers.Utils;
import josiassena.humbug.helpers.callbacks.MessageDeletedListener;
import josiassena.humbug.msg.Message;
import josiassena.humbug.msg.MessageType;

/**
 * File created by josias on 12/30/15.
 */
public class TextMessagesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = TextMessagesAdapter.class.getSimpleName();
    private static final List<MessageDeletedListener> messageDeletedListeners = new ArrayList<>();

    private Activity activity;
    private final List<Message> messageList = new ArrayList<>();

    public TextMessagesAdapter(Activity activity) {
        this.activity = activity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == MessageType.US.getValue()) {
            return new UsViewHolder(
                    LayoutInflater.from(activity).inflate(R.layout.us, parent, false));
        } else {
            return new ThemViewHolder(
                    LayoutInflater.from(activity).inflate(R.layout.them, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int itemViewType = holder.getItemViewType();
        final Message message = messageList.get(position);
        final Bitmap[] images = message.getImages();

        if (itemViewType == MessageType.THEM.getValue()) {
            handleMessageFromThem((josiassena.humbug.messages.view.rec_view.ThemViewHolder) holder,
                    position, message, images);
        } else {
            handleMessageFromUs((josiassena.humbug.messages.view.rec_view.UsViewHolder) holder,
                    position, message, images);
        }
    }

    private void handleMessageFromUs(final UsViewHolder holder, int position, Message message,
                                     Bitmap[] images) {
        holder.messageTv.setText(message.getMessage());
        holder.timeSentReceived.setText(getDate(message.getTimeOfText()));

        if (images.length > 0) {
            // This message has an image. Lets set it to the image view.
            holder.messagePhoto.setVisibility(View.VISIBLE);
            holder.messagePhoto.setImageBitmap(images[0]);
            holder.itemView.setOnClickListener(new MmsOnClickListener(images[0]));
        } else {
            holder.messagePhoto.setVisibility(View.GONE);
        }

        if (message.isFake()) {
            holder.itemView
                    .setOnLongClickListener(new FakeMessageOnLongClickListener(position, message));
        } else {
            holder.itemView.setOnLongClickListener(
                    new DeleteMessageOnLongClickListener(position, message));
        }
    }

    private void handleMessageFromThem(final ThemViewHolder holder, int position, Message message,
                                       final Bitmap[] images) {
        holder.messageTv.setText(message.getMessage());
        holder.timeSentReceived.setText(getDate(message.getTimeOfText()));
        markMessageAsRead(message);

        if (images.length > 0) {
            // This message has an image. Lets set it to the image view.
            holder.messagePhoto.setVisibility(View.VISIBLE);
            holder.messagePhoto.setImageBitmap(images[0]);
            holder.itemView.setOnClickListener(new MmsOnClickListener(images[0]));
        } else {
            holder.messagePhoto.setVisibility(View.GONE);
        }

        holder.itemView
                .setOnLongClickListener(new DeleteMessageOnLongClickListener(position, message));
    }

    private void markMessageAsRead(Message message) {
        // TODO: 1/17/16
//        MmsDatabase.markRead(activity, message.getPhoneNumber());
    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return messageList.get(position).getMessageType().getValue() ==
                MessageType.US.getValue() ? MessageType.US.getValue() : MessageType.THEM.getValue();
    }

    private static String getDate(long timeOfText) {
        Log.d("TextMessagesAdapter", "getDate() called with: milliSeconds = [" + timeOfText + "]");
        Date date = new Date();
        date.setTime(timeOfText);
        return new PrettyTime().format(date);
    }

    public void add(Message message) {
        messageList.add(0, message);
        notifyItemInserted(0);
    }

    public void addAll(ArrayList<Message> messageArrayList) {
        messageList.addAll(0, messageArrayList);
        notifyItemInserted(0);
    }

    private void shareMessage(Message message) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, message.getPhoneNumber());
        sharingIntent.putExtra(Intent.EXTRA_TEXT, message.getMessage());
        activity.startActivity(Intent.createChooser(sharingIntent,
                activity.getResources().getString(R.string.share_using)));
    }

    private void showDeleteDialog(final int position, final Message message) {
        new AlertDialog.Builder(activity)
                .setTitle(activity.getString(R.string.delete_message))
                .setMessage(activity.getString(R.string.are_you_sure))
                .setPositiveButton(activity.getString(R.string.delete),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                messageList.remove(message);
                                notifyItemRemoved(position);

                                removeFromDatabase(message);
//                                removeFromDatabase(message.getMessage(), message.getPhoneNumber());
                            }
                        })
                .setNegativeButton(activity.getString(R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
    }

    public static void addNewMessageDeletedCallback(MessageDeletedListener messageDeletedListener) {
        messageDeletedListeners.add(messageDeletedListener);
    }

    private class DeleteMessageOnLongClickListener implements View.OnLongClickListener {

        private final int position;
        private final Message message;

        DeleteMessageOnLongClickListener(int position, Message message) {
            this.position = position;
            this.message = message;
        }

        /**
         * Called when a view has been clicked and held.
         *
         * @param v The view that was clicked and held.
         * @return true if the callback consumed the long click, false otherwise.
         */
        @Override
        public boolean onLongClick(View v) {
            new BottomSheet.Builder(activity)
                    .sheet(R.menu.message_activity_bottom_sheet_list)
                    .title(R.string.message_options)
                    .listener(new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case R.id.delete_option:
                                    showDeleteDialog(position, message);
                                    break;
                                case R.id.copy_text_option:
                                    Utils.copyTextToClipboard(activity, message.getPhoneNumber(),
                                            message.getMessage());
                                    break;
                                case R.id.share_option:
                                    shareMessage(message);
                                    break;
                            }
                        }
                    }).show();

            return true;
        }

    }

    private class FakeMessageOnLongClickListener implements View.OnLongClickListener {

        private final int position;
        private final Message message;

        FakeMessageOnLongClickListener(int position, Message message) {
            this.position = position;
            this.message = message;
        }

        /**
         * Called when a view has been clicked and held.
         *
         * @param v The view that was clicked and held.
         * @return true if the callback consumed the long click, false otherwise.
         */
        @Override
        public boolean onLongClick(View v) {
            new BottomSheet.Builder(activity)
                    .sheet(R.menu.message_activity_fakesms_bottom_sheet_list)
                    .title(R.string.message_options)
                    .listener(new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case R.id.edit_fake_sms_option:
                                    editFakeSms(position, message);
                                    break;
                                case R.id.delete_option:
                                    showDeleteDialog(position, message);
                                    break;
                                case R.id.copy_text_option:
                                    Utils.copyTextToClipboard(activity, message.getPhoneNumber(),
                                            message.getMessage());
                                    break;
                                case R.id.share_option:
                                    shareMessage(message);
                                    break;
                            }
                        }
                    }).show();
            return true;
        }
    }

    private void editFakeSms(final int position, final Message smsToEdit) {
        View dialogView = activity.getLayoutInflater()
                .inflate(R.layout.fragment_create_fake_sms, null);
        Button fakeSmsTimePickerBtn = (Button) dialogView.findViewById(R.id.fakeSmsTimePickerBtn);
        Button fakeSmsDatePickerBtn = (Button) dialogView.findViewById(R.id.fakeSmsDatePickerBtn);
        final EditText fakeSmsMessage = (EditText) dialogView.findViewById(R.id.fakeSmsMessage);
        final TextView fakeSmsDate = (TextView) dialogView.findViewById(R.id.fakeSmsDate);
        final TextView fakeSmsTime = (TextView) dialogView.findViewById(R.id.fakeSmsTime);
        final Calendar fakeSmsCalendar = Calendar.getInstance();
        fakeSmsCalendar.setTimeInMillis(smsToEdit.getTimeOfText());

        fakeSmsMessage.setText(smsToEdit.getMessage());

        // Date
        Calendar date = Calendar.getInstance();
        SimpleDateFormat dateDateFormat = new SimpleDateFormat("M/d/yyyy");
        date.setTimeInMillis(smsToEdit.getTimeOfText());
        fakeSmsDate.setText(String.format("Date: %s", dateDateFormat.format(date.getTime())));

        // Time
        SimpleDateFormat timeDateFormat = new SimpleDateFormat("h:mm a");
        date.setTimeInMillis(smsToEdit.getTimeOfText());
        fakeSmsTime.setText(String.format("Time: %s", timeDateFormat.format(date.getTime())));

        final int[] fakeSmsYear = new int[1];
        final int[] fakeSmsMonth = new int[1];
        final int[] fakeSmsDay = new int[1];
        final int[] fakeSmsHour = new int[1];
        final int[] fakeSmsMinute = new int[1];

        fakeSmsYear[0] = fakeSmsCalendar.get(Calendar.YEAR);
        fakeSmsMonth[0] = fakeSmsCalendar.get(Calendar.MONTH);
        fakeSmsDay[0] = fakeSmsCalendar.get(Calendar.DAY_OF_MONTH);
        fakeSmsHour[0] = fakeSmsCalendar.get(Calendar.HOUR_OF_DAY);
        fakeSmsMinute[0] = fakeSmsCalendar.get(Calendar.MINUTE);

        final Calendar currentDate = Calendar.getInstance();
        fakeSmsDatePickerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(activity, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        Log.d(TAG, "onDateSet() called with: " + "view = [" + view + "], year = [" +
                                year + "], monthOfYear = [" + monthOfYear + "], dayOfMonth = [" +
                                dayOfMonth + "]");
                        fakeSmsYear[0] = year;
                        fakeSmsMonth[0] = monthOfYear;
                        fakeSmsDay[0] = dayOfMonth;

                        Calendar dateCal = Calendar.getInstance();
                        dateCal.set(year, monthOfYear, dayOfMonth);

                        SimpleDateFormat dateFormat = new SimpleDateFormat("M/d/yyyy");
                        fakeSmsDate.setText(
                                String.format("Date: %s", dateFormat.format(dateCal.getTime())));
                    }
                }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH),
                        currentDate.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        fakeSmsTimePickerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(activity, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        Log.d(TAG, "onTimeSet() called with: " + "view = [" + view +
                                "], hourOfDay = [" + hourOfDay + "], minute = [" + minute + "]");
                        fakeSmsHour[0] = hourOfDay;
                        fakeSmsMinute[0] = minute;

                        Calendar timeCal = Calendar.getInstance();
                        timeCal.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        timeCal.set(Calendar.MINUTE, minute);

                        SimpleDateFormat dateFormat = new SimpleDateFormat("h:mm a");
                        fakeSmsTime.setText(
                                String.format("Time: %s", dateFormat.format(timeCal.getTime())));
                    }
                }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), false).show();
            }
        });

        new AlertDialog.Builder(activity)
                .setTitle(activity.getString(R.string.create_fake_sms))
                .setView(dialogView)
                .setPositiveButton(activity.getString(R.string.send),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                if (!fakeSmsMessage.getText().toString().trim().isEmpty()) {
                                    fakeSmsCalendar
                                            .set(fakeSmsYear[0], fakeSmsMonth[0], fakeSmsDay[0],
                                                    fakeSmsHour[0], fakeSmsMinute[0],
                                                    fakeSmsCalendar.get(Calendar.SECOND));

                                    Message fakeSms = new Message();
                                    fakeSms.setPhoneNumber(smsToEdit.getPhoneNumber());
                                    fakeSms.setMessage(fakeSmsMessage.getText().toString());
                                    fakeSms.setTimeOfText(fakeSmsCalendar.getTimeInMillis());
                                    fakeSms.setMessageType(MessageType.US);
                                    fakeSms.setFake(true);

                                    sendFakeMessage(activity, fakeSms);

                                    messageList.add(0, fakeSms);
                                    messageList.remove(smsToEdit);

                                    removeFromDatabase(smsToEdit);
//                            notifyDataSetChanged();
                                    notifyItemRemoved(position);
                                } else {
                                    Toast.makeText(activity, R.string.please_enter_msg,
                                            Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    private void removeFromDatabase(Message message) {
        final ContentResolver contentResolver = activity.getContentResolver();

        final String selection =
                "date like '%" + message.getTimeOfText() +
                        "%' AND address like '%" + message.getPhoneNumber() +
                        "%' AND body like '%" + message.getMessage() + "%'" + "";

        final Uri smsUri = Telephony.Sms.CONTENT_URI;
        final Cursor cursor = contentResolver.query(smsUri, null, selection, null, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                contentResolver.delete(smsUri, selection, null);
                cursor.close();
            } else {
                Log.e(TAG, "removeFromDatabase: there are no items in this cursor");
            }
        } else {
            Log.e(TAG, "removeFromDatabase: cursor is null");
        }

        for (MessageDeletedListener listener : messageDeletedListeners) {
            listener.onNewMessageDeletedListener();
        }
    }

    private void sendFakeMessage(Context context, Message message) {
        Log.d(TAG,
                "sendFakeMessage() called with: " + "context = [" + context + "], simpleSms = [" +
                        message + "]");
        ContentValues contentValues = new ContentValues();
        contentValues.put(Telephony.Sms.ADDRESS, message.getPhoneNumber());
        contentValues.put(Telephony.Sms.BODY, message.getMessage());
        contentValues.put(Telephony.Sms.TYPE, message.getMessageType().getValue());
        contentValues.put(Telephony.Sms.DATE, message.getTimeOfText());
        contentValues.put(Telephony.Sms.DATE_SENT, message.getTimeOfText());
        contentValues.put(Telephony.Sms.LOCKED, message.isFake());

        Log.d(TAG, "sendFakeMessage: " + contentValues.toString());
        context.getContentResolver().insert(Telephony.Sms.Sent.CONTENT_URI, contentValues);
    }

    private class MmsOnClickListener implements View.OnClickListener {

        private Bitmap image;

        public MmsOnClickListener(Bitmap image) {
            this.image = image;
        }

        /**
         * Called when a view has been clicked.
         *
         * @param v The view that was clicked.
         */
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(activity, ImageViewActivity.class);
            intent.putExtra("image", image);
            activity.startActivity(intent);
        }
    }
}
