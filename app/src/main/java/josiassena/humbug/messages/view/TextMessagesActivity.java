package josiassena.humbug.messages.view;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.NotificationManager;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.hannesdorfmann.mosby.mvp.MvpActivity;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.listener.single.DialogOnDeniedPermissionListener;
import com.karumi.dexter.listener.single.PermissionListener;
import com.rockerhieu.emojicon.EmojiconEditText;
import com.rockerhieu.emojicon.EmojiconGridFragment;
import com.rockerhieu.emojicon.EmojiconsFragment;
import com.rockerhieu.emojicon.emoji.Emojicon;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import josiassena.humbug.R;
import josiassena.humbug.contact_information.view.ContactInformationActivity;
import josiassena.humbug.helpers.HumBugSharedPreferences;
import josiassena.humbug.helpers.Utils;
import josiassena.humbug.helpers.broadcast_receivers.mms.IncomingMmsBroadCastReceiver;
import josiassena.humbug.helpers.broadcast_receivers.sms.IncomingSmsBroadCastReceiver;
import josiassena.humbug.helpers.callbacks.NewMessageReceivedListener;
import josiassena.humbug.main.view.MainActivity;
import josiassena.humbug.messages.presenter.TextMessagesPresenterImpl;
import josiassena.humbug.messages.send_sms.SendMmsActivity;
import josiassena.humbug.messages.view.rec_view.TextMessagesAdapter;
import josiassena.humbug.msg.Conversation;
import josiassena.humbug.msg.Message;
import josiassena.humbug.msg.MessageType;

/**
 * File created by josias on 12/30/15.
 */
public class TextMessagesActivity extends MvpActivity<TextMessagesView, TextMessagesPresenterImpl>
        implements TextMessagesView, NewMessageReceivedListener,
        EmojiconGridFragment.OnEmojiconClickedListener,
        EmojiconsFragment.OnEmojiconBackspaceClickedListener {

    private static final String TAG = TextMessagesActivity.class.getSimpleName();

    private static final int OPEN_PICTURE_GALLERY_REQUEST_CODE = 2132;
    private static final int OPEN_VIDEO_GALLERY_REQUEST_CODE = 214;
    private static final int ADD_NEW_CONTACT_REQUEST_CODE = 9476;
    private static final int OPEN_RECORDER_REQUEST_CODE = 980;
    private static final int OPEN_CAMERA_REQUEST_CODE = 4567;
    private static final int SENDING_SMS_REQUEST_CODE = 1111;
    private static final int PICK_CONTACT = 9811;

    @Bind (R.id.emojiView)
    FrameLayout emojiView;

    @Bind (R.id.messageToSendEt)
    EmojiconEditText messageToSendEt;

    @Bind (R.id.textMessageRecView)
    RecyclerView textMessageRecView;

    @Bind (R.id.personToMessageSearchView)
    SearchView searchView;

    @Bind (R.id.sendNewMsgTopView)
    LinearLayout sendNewMsgTopView;

    @Bind (R.id.addEmojiBtn)
    ImageView addEmojiBtn;

    @Bind (R.id.imageAttachment)
    ImageView imageAttachment;

    private boolean comingFromNotification = false;
    private boolean creatingNewConversation;
    private boolean isNewContact = false;

    private Message newConversationSms = new Message();
    private String notificationPhoneNumber;
    private TextMessagesAdapter adapter;
    private Conversation conversation;

    @NonNull
    public TextMessagesPresenterImpl createPresenter() {
        return new TextMessagesPresenterImpl();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_messages);
        ButterKnife.bind(this);

        IncomingSmsBroadCastReceiver.addNewMessageCallBack(this);
        IncomingMmsBroadCastReceiver.addNewMessageCallBack(this);

        init();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult() called with: " + "requestCode = [" + requestCode +
                "], resultCode = [" + resultCode + "], data = [" + data + "]");

        switch (requestCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();
                    Cursor cursor = getContentResolver().query(contactData, null, null, null, null);

                    if (cursor != null && cursor.moveToFirst()) {
                        String name = cursor.getString(
                                cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                        String number = Utils.getContactPhoneNumberFromName(this, name);

                        newConversationSms = new Message();
                        newConversationSms.setName(name);
                        newConversationSms.setMessageType(MessageType.US);
                        newConversationSms
                                .setTimeOfText(GregorianCalendar.getInstance().getTimeInMillis());
                        newConversationSms.setPhoneNumber(number);

                        searchView.setQuery(name, false);
                        cursor.close();
                    }
                }
                break;
            case ADD_NEW_CONTACT_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();
                    Cursor cursor = getContentResolver().query(contactData, null, null, null, null);

                    if (cursor != null && cursor.moveToFirst()) {
                        String name = cursor.getString(
                                cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                        conversation.setTitle(name);

                        setupActionBar();
                        isNewContact = false;
                        invalidateOptionsMenu();

                        cursor.close();
                    }
                }
                break;
            case OPEN_PICTURE_GALLERY_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    handleOpenPictureGalleryRequestCode(data);
                }
                break;
            case OPEN_VIDEO_GALLERY_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    // TODO: 1/2/16
                }
                break;
            case OPEN_CAMERA_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    // TODO: 1/2/16
                }
                break;
            case OPEN_RECORDER_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    // TODO: 1/2/16
                }
                break;
            case Crop.REQUEST_CROP:
                handleCrop(data);
                break;
        }
    }

    private void handleOpenPictureGalleryRequestCode(Intent data) {
        boolean comingFromNotification = getIntent()
                .getBooleanExtra(HumBugSharedPreferences.COMING_FROM_NOTIFICATION, false);

        if (comingFromNotification) {
            data.putExtra("phone_number",
                    getIntent().getStringExtra(HumBugSharedPreferences.NOTIFICATION_PHONE_NUMBER));
        } else {
            Bundle extras = getIntent().getExtras();

            if (extras != null) {
                conversation = extras.getParcelable("conversation");

                if (conversation != null) {
                    data.putExtra("phone_number", conversation.getPhoneNumber());
                }
            }
        }

        data.setClass(this, SendMmsActivity.class);
        startActivityForResult(data, SENDING_SMS_REQUEST_CODE);
    }

    /**
     * Start the cropping view so that the user can start cropping the selected photo.
     */
    private void startCropping(Uri source) {
        File imageFile = new File(getCacheDir(), "humbug_cropped_photo");
        Crop.of(source, Uri.fromFile(imageFile)).start(this);
    }

    /**
     * @param result - Intent sent back by the cropping view.
     */
    // TODO: 1/2/16  FIXME: 1/2/16
    private void handleCrop(Intent result) {
        Log.d(TAG, "handleCrop() called with: " + "result = [" + result + "]");
        Uri croppedImageDestination = Crop.getOutput(result);
        imageAttachment.setVisibility(View.VISIBLE);
        imageAttachment.setImageURI(croppedImageDestination);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);

        if (conversation != null) {
            intent.putExtra(HumBugSharedPreferences.CONVO_TO_MARK_READ,
                    conversation.getPhoneNumber());
            setResult(TextMessagesActivity.RESULT_OK, intent);
            finishActivity(HumBugSharedPreferences.MARKING_CONVO_READ);
            finish();
        } else if (comingFromNotification) {
            intent.putExtra(HumBugSharedPreferences.CONVO_TO_MARK_READ, notificationPhoneNumber);
            startActivity(intent);
        } else {
            startActivity(intent);
        }

        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        Log.d(TAG, "onCreateOptionsMenu() called with: " + "menu = [" + menu + "]");
        getMenuInflater().inflate(R.menu.menu_text_messages_activity, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        Log.d(TAG, "onPrepareOptionsMenu() called with: " + "menu = [" + menu + "]");

        MenuItem addNewContactItem = menu.findItem(R.id.action_add_contact);
        MenuItem moreInfoItem = menu.findItem(R.id.action_more_info);

        if (isNewContact) {
            // show add new contact option and hide more info option
            addNewContactItem.setVisible(true);
            moreInfoItem.setVisible(false);
        } else {
            // hide add new contact option and show more info option
            addNewContactItem.setVisible(false);
            moreInfoItem.setVisible(true);
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        switch (itemId) {
            case R.id.action_send_fake_sms:
                showSendFakeSmsDialog();
                break;
            case R.id.action_call:
                PermissionListener dialogPermissionListener =
                        DialogOnDeniedPermissionListener.Builder
                                .withContext(this)
                                .withTitle("Call permission")
                                .withMessage("Call permission is needed make calls")
                                .withButtonText(android.R.string.ok)
                                .withIcon(R.drawable.icon)
                                .build();

                Dexter.checkPermission(dialogPermissionListener, Manifest.permission.CALL_PHONE);

                String phoneNumber = conversation.getPhoneNumber();
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + phoneNumber));
                startActivity(callIntent);
                break;
            case R.id.action_more_info:
                Intent intent = new Intent(this, ContactInformationActivity.class);

                Bundle bundle = new Bundle();
                bundle.putParcelable("conversation", conversation);

                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case R.id.action_add_contact:
                Utils.openAddNewContactScreen(this, conversation.getPhoneNumber(),
                        ADD_NEW_CONTACT_REQUEST_CODE);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void init() {
        getExtras();
        initRecView();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.emojiView, EmojiconsFragment.newInstance(false)).commit();
        HumBugSharedPreferences.setContext(this);

        creatingNewConversation = getIntent()
                .getBooleanExtra(HumBugSharedPreferences.STARTING_NEW_CONVERSATION, false);
        if (creatingNewConversation) {
            sendNewMsgTopView.setVisibility(View.VISIBLE);
            hideActionBar();
            initSearchView();
        } else {
            sendNewMsgTopView.setVisibility(View.GONE);
        }

        messageToSendEt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (emojiView.isShown()) {
                    emojiView.setVisibility(View.GONE);
                    addEmojiBtn.setImageDrawable(
                            getResources().getDrawable(R.drawable.ic_insert_emoticon));
                }
                return false;
            }
        });
    }

    private void initSearchView() {
        searchView.setOnQueryTextListener(new SearchQueryTextListener());
        searchView.setOnSuggestionListener(new SearchQuerySuggestionListener());
    }

    private void getExtras() {
        comingFromNotification = getIntent()
                .getBooleanExtra(HumBugSharedPreferences.COMING_FROM_NOTIFICATION, false);
        Bundle extras = getIntent().getExtras();

        if (comingFromNotification) {
            Log.d(TAG, "getExtras() came from notification!");
            handleComingFromNotification(getIntent());
        } else {
            if (extras != null) {
                Log.d(TAG, "getExtras() did not come from notification, proceeding normally. ");
                conversation = extras.getParcelable("conversation");

                if (conversation != null) {
                    setupActionBar();
                    Utils.markMessagesRead(this, conversation.getPhoneNumber());
                    presenter.getTextMessages(this, this, conversation);
                }
            } else {
                Log.e(TAG, "Extras are null.");
            }
        }
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setBackgroundDrawable(
                    new ColorDrawable(HumBugSharedPreferences.getToolBarBackGroundColor()));

            if (conversation.getTitle() != null && !conversation.getTitle().trim().isEmpty()) {
                actionBar.setTitle(conversation.getTitle());
                actionBar.setTitle(Html.fromHtml("<font face='sans-serif-light' color='" +
                        HumBugSharedPreferences.getToolBarTextColor() + "'>" +
                        conversation.getTitle() +
                        "</font>"));
                isNewContact = false;
            } else {
                // this means that this person is a new contact
                actionBar.setTitle(Html.fromHtml("<font face='sans-serif-light' color='" +
                        HumBugSharedPreferences.getToolBarTextColor() + "'>" +
                        conversation.getPhoneNumber() +
                        "</font>"));
                isNewContact = true;
            }
        }
    }

    private void handleComingFromNotification(Intent intent) {
        Log.d(TAG, "handleComingFromNotification() called with: " + "intent = [" + intent + "]");
        dismissNotifications();

        notificationPhoneNumber = intent
                .getStringExtra(HumBugSharedPreferences.NOTIFICATION_PHONE_NUMBER);
        String contactName = Utils.getContactName(this, notificationPhoneNumber);

        Utils.markMessagesRead(this, notificationPhoneNumber);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            if (contactName != null && !contactName.trim().isEmpty()) {
                actionBar.setTitle(contactName);
            } else {
                actionBar.setTitle(notificationPhoneNumber);
            }
        }

        presenter.getTextMessages(this, this, notificationPhoneNumber);
    }

    private void dismissNotifications() {
        NotificationManager notificationManager = (NotificationManager) getSystemService(
                Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(HumBugSharedPreferences.NEW_MESSAGE_NOTIFICATION_ID);
    }

    private void initRecView() {
        adapter = new TextMessagesAdapter(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, true);

        textMessageRecView.setItemViewCacheSize(200);
        textMessageRecView.setLayoutManager(linearLayoutManager);
        textMessageRecView.setAdapter(adapter);
        textMessageRecView.scrollToPosition(0);
    }

    public void onEmojiconBackspaceClicked(View paramView) {
        EmojiconsFragment.backspace(messageToSendEt);
    }

    public void onEmojiconClicked(Emojicon paramEmojicon) {
        EmojiconsFragment.input(messageToSendEt, paramEmojicon);
    }

    private void showSendFakeSmsDialog() {
        View dialogView = getLayoutInflater().inflate(R.layout.fragment_create_fake_sms, null);
        final EditText fakeSmsMessage = (EditText) dialogView.findViewById(R.id.fakeSmsMessage);
        Button fakeSmsTimePickerBtn = (Button) dialogView.findViewById(R.id.fakeSmsTimePickerBtn);
        Button fakeSmsDatePickerBtn = (Button) dialogView.findViewById(R.id.fakeSmsDatePickerBtn);
        final TextView fakeSmsDate = (TextView) dialogView.findViewById(R.id.fakeSmsDate);
        final TextView fakeSmsTime = (TextView) dialogView.findViewById(R.id.fakeSmsTime);

        final int[] fakeSmsYear = new int[1];
        final int[] fakeSmsMonth = new int[1];
        final int[] fakeSmsDay = new int[1];
        final int[] fakeSmsHour = new int[1];
        final int[] fakeSmsMinute = new int[1];

        final Calendar currentDate = Calendar.getInstance();
        fakeSmsDatePickerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(TextMessagesActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                  int dayOfMonth) {
                                Log.d(TAG, "onDateSet() called with: " + "view = [" + view +
                                        "], year = [" + year + "], monthOfYear = [" + monthOfYear +
                                        "], dayOfMonth = [" + dayOfMonth + "]");
                                fakeSmsYear[0] = year;
                                fakeSmsMonth[0] = monthOfYear;
                                fakeSmsDay[0] = dayOfMonth;

                                Calendar dateCal = Calendar.getInstance();
                                dateCal.set(year, monthOfYear, dayOfMonth);

                                SimpleDateFormat dateFormat = new SimpleDateFormat("M/d/yyyy");
                                fakeSmsDate.setText(String.format("Date: %s",
                                        dateFormat.format(dateCal.getTime())));
                            }
                        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH),
                        currentDate.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        fakeSmsTimePickerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(TextMessagesActivity.this,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                Log.d(TAG, "onTimeSet() called with: " + "view = [" + view +
                                        "], hourOfDay = [" + hourOfDay + "], minute = [" + minute +
                                        "]");
                                fakeSmsHour[0] = hourOfDay;
                                fakeSmsMinute[0] = minute;

                                Calendar timeCal = Calendar.getInstance();
                                timeCal.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                timeCal.set(Calendar.MINUTE, minute);

                                SimpleDateFormat dateFormat = new SimpleDateFormat("h:mm a");
                                fakeSmsTime.setText(String.format("Time: %s",
                                        dateFormat.format(timeCal.getTime())));
                            }
                        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), false)
                        .show();
            }
        });

        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.create_fake_sms))
                .setView(dialogView)
                .setPositiveButton(getString(R.string.send), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if (!fakeSmsMessage.getText().toString().trim().isEmpty()) {
                            Calendar calendar = Calendar.getInstance();
                            calendar.set(fakeSmsYear[0], fakeSmsMonth[0], fakeSmsDay[0],
                                    fakeSmsHour[0], fakeSmsMinute[0],
                                    calendar.get(Calendar.SECOND));

                            String phoneNumber;
                            if (conversation != null) {
                                phoneNumber = conversation.getPhoneNumber();
                            } else {
                                phoneNumber = notificationPhoneNumber;
                            }

                            Message fakeSms = new Message();
                            fakeSms.setPhoneNumber(phoneNumber);
                            fakeSms.setMessage(fakeSmsMessage.getText().toString());
                            fakeSms.setTimeOfText(calendar.getTimeInMillis());
                            fakeSms.setMessageType(MessageType.US);
                            fakeSms.setFake(true);

                            presenter.sendFakeMessage(TextMessagesActivity.this, fakeSms);
                            adapter.add(fakeSms);
                        } else {
                            Toast.makeText(TextMessagesActivity.this, R.string.please_enter_msg,
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    private void hideActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
    }

    private void sendNewMessageFromSearchedContact() {
        // We are creating a new conversation
        newConversationSms.setMessage(messageToSendEt.getText().toString());

        adapter.add(newConversationSms);
        adapter.notifyDataSetChanged();
        presenter.sendMessage(this, newConversationSms);
        messageToSendEt.setText("");

        sendNewMsgTopView.setVisibility(View.GONE);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            if (newConversationSms.getName() != null &&
                    !newConversationSms.getName().trim().isEmpty()) {
                actionBar.setTitle(
                        newConversationSms.getName() + " (" + newConversationSms.getPhoneNumber() +
                                ")");
            } else {
                actionBar.setTitle(newConversationSms.getPhoneNumber());
            }
        }
    }

    private void sendNewMessage() {
        Message message = new Message();
        message.setMessageType(MessageType.US);

        if (conversation != null) {
            message.setPhoneNumber(conversation.getPhoneNumber());
        } else {
            message.setPhoneNumber(notificationPhoneNumber);
        }

        message.setMessage(messageToSendEt.getText().toString());
        message.setTimeOfText(GregorianCalendar.getInstance().getTimeInMillis());

        adapter.add(message);
        textMessageRecView.smoothScrollToPosition(0);
        presenter.sendMessage(this, message);
        messageToSendEt.setText("");
    }

    @OnClick (R.id.sendNewMsgBtn)
    public void sendNewMessageClick() {
        if (!messageToSendEt.getText().toString().trim().isEmpty()) {
            if (!creatingNewConversation) {
                sendNewMessage();
            } else {
                sendNewMessageFromSearchedContact();
            }
        }
    }

    // TODO: 1/22/16 - Enable this once MMS functionality has been implemented
//    @OnClick(R.id.addItem)
//    public void addItemButtonClicked() {
//        new BottomSheet.Builder(this)
//                .sheet(R.menu.attach_to_message)
//                .title(R.string.attach)
//                .grid()
//                .listener(new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        switch (which) {
//                            case R.id.choose_photo_option:
//                                presenter.openPictureGallery(TextMessagesActivity.this, OPEN_PICTURE_GALLERY_REQUEST_CODE);
//                                break;
//                            case R.id.choose_video_option:
//                                presenter.openVideoGallery(TextMessagesActivity.this, OPEN_VIDEO_GALLERY_REQUEST_CODE);
//                                break;
//                            case R.id.take_picture_option:
//                                presenter.openCameraToTakePicture(TextMessagesActivity.this, OPEN_CAMERA_REQUEST_CODE);
//                                break;
//                            case R.id.record_video_option:
//                                presenter.openCameraToRecordVideo(TextMessagesActivity.this, OPEN_RECORDER_REQUEST_CODE);
//                                break;
//                        }
//                    }
//                }).show();
//    }

    @OnClick (R.id.addEmojiBtn)
    public void addEmojiBtnClick() {
        Utils.hideSoftKeyboard(this);

        if (!emojiView.isShown()) {
            emojiView.setVisibility(View.VISIBLE);
            addEmojiBtn.setImageDrawable(getResources().getDrawable(R.drawable.ic_close));
        } else {
            emojiView.setVisibility(View.GONE);
            addEmojiBtn.setImageDrawable(getResources().getDrawable(R.drawable.ic_insert_emoticon));
        }
    }

    @OnClick (R.id.addPersonBtn)
    public void addPersonBtnClick() {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, PICK_CONTACT);
    }

    @OnClick (R.id.imageAttachment)
    public void imageAttachmentClick() {
        if (imageAttachment.isShown()) {
            new AlertDialog.Builder(this)
                    .setTitle("Remove attachment")
                    .setMessage("Do you want to remove this attachment?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            imageAttachment.setImageURI(null);
                            imageAttachment.setVisibility(View.GONE);
                        }
                    })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
        }
    }

    @Override
    public void onNewMessagesReceived(final ArrayList<Message> messages) {

        if (!comingFromNotification) {
            boolean isForUs = false;

            for (Message message : messages) {
                if (conversation.getPhoneNumber().equals(message.getPhoneNumber())) {
                    isForUs = true;
                }
            }

            if (isForUs) {
                adapter.addAll(messages);
                textMessageRecView.smoothScrollToPosition(0);
            }
        } else {
            adapter.addAll(messages);
            textMessageRecView.smoothScrollToPosition(0);
        }
    }

    private class SearchQueryTextListener implements SearchView.OnQueryTextListener {

        /**
         * Called when the user submits the query. This could be due to a key press on the keyboard
         * or due to pressing a submit button. The listener can override the standard behavior by
         * returning true to indicate that it has handled the submit request. Otherwise return false
         * to let the SearchView handle the submission by launching any associated intent.
         *
         * @param query the query text that is to be submitted
         * @return true if the query has been handled by the listener, false to let the SearchView
         * perform the default action.
         */
        @Override
        public boolean onQueryTextSubmit(String query) {
            // Do this when we press enter or submit
            loadContacts(query);
            return false;
        }

        /**
         * Called when the query text is changed by the user.
         *
         * @param query the new content of the query text field.
         * @return false if the SearchView should perform the default action of showing any
         * suggestions if available, true if the action was handled by the listener.
         */
        @Override
        public boolean onQueryTextChange(String query) {
            loadContacts(query);
            return false;
        }
    }

    private class SearchQuerySuggestionListener implements SearchView.OnSuggestionListener {

        /**
         * Called when a suggestion was selected by navigating to it.
         *
         * @param position the absolute position in the list of suggestions.
         * @return true if the listener handles the event and wants to override the default behavior
         * of possibly rewriting the query based on the selected item, false otherwise.
         */
        @Override
        public boolean onSuggestionSelect(int position) {
            return false;
        }

        /**
         * Called when a suggestion was clicked.
         *
         * @param position the absolute position of the clicked item in the list of suggestions.
         * @return true if the listener handles the event and wants to override the default behavior
         * of launching any intent or submitting a search query specified on that item. Return false
         * otherwise.
         */
        @Override
        public boolean onSuggestionClick(int position) {
            Log.d(TAG, "onSuggestionClick() called with: " + "position = [" + position + "]");

            // what do we do when we click on the suggestion shown?
            CursorAdapter cursorAdapter = searchView.getSuggestionsAdapter();
            Cursor cursorAdapterCursor = cursorAdapter.getCursor();

            // Move to the selected contact
            cursorAdapterCursor.moveToPosition(position);

            // Get the _ID value
            long contactId = cursorAdapterCursor
                    .getLong(cursorAdapterCursor.getColumnIndex(ContactsContract.Contacts._ID));

            // Get the selected LOOKUP KEY
            String contactKey = cursorAdapterCursor.getString(
                    cursorAdapterCursor.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));

            // Contact phone number
            String contactPhoneNumber = cursorAdapterCursor.getString(cursorAdapterCursor
                    .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

            // Create the contact's content Uri
            Uri contactUri = ContactsContract.Contacts.getLookupUri(contactId, contactKey);
            Cursor contactCursor = getContentResolver().query(contactUri, null, null, null, null);

            if (contactCursor != null && contactCursor.moveToFirst()) {
                while (!contactCursor.isAfterLast()) {
                    String contactName = contactCursor.getString(
                            contactCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                    newConversationSms.setName(contactName);
                    newConversationSms.setMessageType(MessageType.US);
                    newConversationSms
                            .setTimeOfText(GregorianCalendar.getInstance().getTimeInMillis());
                    newConversationSms.setPhoneNumber(contactPhoneNumber);

                    searchView.setQuery(contactName, false);
                    contactCursor.moveToNext();
                }
                contactCursor.close();
            }

            return true;
        }
    }

    private void loadContacts(String query) {
        Log.d(TAG, "loadContacts() called with: " + "query = [" + query + "]");

        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String[] projection = new String[] {
                ContactsContract.Contacts._ID,
                Build.VERSION.SDK_INT
                        >= Build.VERSION_CODES.HONEYCOMB ?
                        ContactsContract.Contacts.DISPLAY_NAME_PRIMARY :
                        ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.Contacts.LOOKUP_KEY,
                ContactsContract.CommonDataKinds.Phone.NUMBER};

        String[] FROM_COLUMNS = {Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ?
                ContactsContract.Contacts.DISPLAY_NAME_PRIMARY : ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER};

        int[] TO_IDS = {R.id.contactName, R.id.contactNumber};

        String selection =
                "display_name like '%" + query + "%' OR " + "data1 like '%" + query + "%'";
        Cursor people = getContentResolver().query(uri, projection, selection, null, null);

        searchView.setSuggestionsAdapter(
                new SimpleCursorAdapter(
                        this,
                        R.layout.search_contacts_item,
                        people,
                        FROM_COLUMNS,
                        TO_IDS));
    }

}