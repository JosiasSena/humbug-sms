package josiassena.humbug.messages.send_sms;

import android.content.ClipData;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import josiassena.humbug.R;
import josiassena.humbug.helpers.Utils;
import josiassena.humbug.helpers.database.MmsDatabase;
import josiassena.humbug.msg.Message;
import josiassena.humbug.msg.MessageType;

public class SendMmsActivity extends AppCompatActivity {

    private static final String TAG = "SendSmsActivity";
    private static final int ADDING_MORE_IMAGES_REQUEST_CODE = 555;

    private ArrayList<ImageView> imageViews = new ArrayList<>();
    private ArrayList<ClipData.Item> images = new ArrayList<>();

    @Bind(R.id.imagePreview)
    ImageView imagePreview;

    @Bind(R.id.horizontalGallery)
    LinearLayout horizontalGallery;

    @Bind(R.id.messageToSend)
    EditText messageToSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_sms);
        ButterKnife.bind(this);
        getData(getIntent());
    }

    private void getData(Intent intent) {
        Log.d(TAG, "getData() called.");

        ClipData clipData = intent.getClipData();

        if (clipData != null) {
            int count = clipData.getItemCount();

            ClipData.Item firstImage = clipData.getItemAt(0);
            Picasso.with(this)
                    .load(firstImage.getUri())
                    .fit()
                    .centerInside()
                    .into(imagePreview);

            createImageViews(count);
            for (int i = 0; i < count; i++) {
                ClipData.Item item = clipData.getItemAt(i);
                images.add(item);
            }

        } else {
            Uri uri = getIntent().getData();
            Picasso.with(this)
                    .load(uri)
                    .fit()
                    .centerInside()
                    .into(imagePreview);

            createImageViews(1);
            for (int i = 0; i < 1; i++) {
                ClipData.Item item = new ClipData.Item(uri);
                images.add(item);
            }
        }

        for (int i = 0; i < images.size(); i++) {
            Picasso.with(this)
                    .load(images.get(i).getUri())
                    .resize(100, 100)
                    .centerCrop()
                    .into(imageViews.get(i));

            imageViews.get(i).setOnClickListener(new ImageOnClickListener(i));
        }
    }

    private void createImageViews(int count) {
        int heightWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, getResources().getDisplayMetrics());
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(heightWidth, heightWidth);

        for (int i = 0; i < count; i++) {
            ImageView imageView = new ImageView(this);
            imageView.setLayoutParams(layoutParams);
            imageView.setMaxHeight(heightWidth);
            imageView.setMaxWidth(heightWidth);

            imageViews.add(imageView);
            horizontalGallery.addView(imageView);
        }
    }

    private class ImageOnClickListener implements View.OnClickListener {
        private int position;

        public ImageOnClickListener(int position) {
            this.position = position;
        }

        /**
         * Called when a view has been clicked.
         *
         * @param v The view that was clicked.
         */
        @Override
        public void onClick(View v) {
            Picasso.with(SendMmsActivity.this)
                    .load(images.get(position).getUri())
                    .fit()
                    .centerInside()
                    .into(imagePreview);
        }
    }

    @OnClick(R.id.sendMmsMessage)
    public void sendMmsMessageClick() {
        Message message = new Message();
        message.setMessage(messageToSend.getText().toString());
        message.setMessageType(MessageType.US);

        Bitmap[] bitmaps = new Bitmap[images.size()];
        for (int i = 0; i < images.size(); i++) {
            ClipData.Item item = images.get(i);
            Uri imageUri = item.getUri();

            Bitmap bitmap;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                bitmaps[i] = bitmap;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        message.setImages(bitmaps);

        String phoneNumber = getIntent().getStringExtra("phone_number");
        message.setPhoneNumber(phoneNumber);
        message.setName(Utils.getContactName(this, phoneNumber));
        message.setTimeOfText(GregorianCalendar.getInstance().getTimeInMillis());

        sendMMS(message);
        addMmsToDatabase(message);
    }

    // TODO: 1/17/16
    private void sendMMS(Message message) {
//        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, new Intent(Constants.ACTION_MMS_SENT), 0);
//
//        final byte[] pdu = buildPdu(MmsMessagingDemo.this, recipients, subject, text);
//        Uri writerUri = (new Uri.Builder())
//                .authority("com.example.android.apis.os.MmsFileProvider")
//                .path(fileName)
//                .scheme(ContentResolver.SCHEME_CONTENT)
//                .build();
//
//        FileOutputStream writer = null;
//        Uri contentUri = null;
//        try {
//            writer = new FileOutputStream(mSendFile);
//            writer.write(pdu);
//            contentUri = writerUri;
//        } catch (final IOException e) {
//            Log.e(TAG, e.getMessage(), e);
//        } finally {
//            if (writer != null) {
//                try {
//                    writer.close();
//                } catch (IOException e) {
//                    Log.e(TAG, e.getMessage(), e);
//                }
//            }
//        }
//
//        SmsManager.getDefault().sendMultimediaMessage(this, contentUri, null, null, pendingIntent);

//        Intent sendMmsIntent = new Intent(Intent.ACTION_SEND, Uri.parse("content://media/external/images/media/1"));
//        sendMmsIntent.putExtra("sms_body", message.getMessage());
//        sendMmsIntent.putExtra("address", message.getPhoneNumber());
//        sendMmsIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("content://media/external/images/media/1"));
//        sendMmsIntent.setMessageType("image/jpeg");
//        startActivity(sendMmsIntent);
    }

    @NonNull
    private String getAuthority() {
        return "com.example.android.apis.os.MmsFileProvider";
    }

    private void addMmsToDatabase(Message message) {
        Bitmap[] images = message.getImages();

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        images[0].compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteArray = stream.toByteArray();

        MmsDatabase.insertSentMms(this, MmsDatabase.MESSAGE_BOX_OUTBOX,
                new String[]{message.getPhoneNumber()},
                "You received a picture.",
                byteArray, message.getTimeOfText());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_send_sms, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        switch (itemId) {
            case R.id.action_add_photo:
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), ADDING_MORE_IMAGES_REQUEST_CODE);
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult() called with: " + "requestCode = [" + requestCode + "], resultCode = [" + resultCode + "], data = [" + data + "]");

        if (resultCode == SendMmsActivity.RESULT_OK && requestCode == ADDING_MORE_IMAGES_REQUEST_CODE) {
            getData(data);
        }
    }
}
