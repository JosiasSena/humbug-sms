package josiassena.humbug.helpers.callbacks;

/**
 * File created by josias on 1/1/16.
 */
public interface MessageDeletedListener {

    void onNewMessageDeletedListener();
}
