package josiassena.humbug.helpers.callbacks;

import java.util.ArrayList;

import josiassena.humbug.msg.Message;

/**
 * File created by josias on 12/30/15.
 */
public interface NewMessageReceivedListener {

    void onNewMessagesReceived(ArrayList<Message> messages);
}
