package josiassena.humbug.helpers.callbacks;

/**
 * File created by josias on 12/30/15.
 */
public interface NewMessageSentListener {

    void onNewMessageSent();
}
