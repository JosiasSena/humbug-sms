package josiassena.humbug.helpers.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Telephony;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import josiassena.humbug.msg.MessageConstants;

/**
 * File created by josias on 1/17/16.
 */
public class MmsDatabase {

    private static final String TAG = MmsDatabase.class.getSimpleName();

    /**
     * Message box: all messages.
     */
    public static final int MESSAGE_BOX_ALL = 0;

    /**
     * Message box: inbox.
     */
    public static final int MESSAGE_BOX_INBOX = 1;

    /**
     * Message box: sent messages.
     */
    public static final int MESSAGE_BOX_SENT = 2;

    /**
     * Message box: drafts.
     */
    public static final int MESSAGE_BOX_DRAFTS = 3;

    /**
     * Message box: outbox.
     */
    public static final int MESSAGE_BOX_OUTBOX = 4;

    /**
     * Message box: failed.
     */
    public static final int MESSAGE_BOX_FAILED = 5;

    /**
     * Message box: queued.
     */
    public static final int MESSAGE_TYPE_QUEUED = 6;

    public static final int MESSAGE_UNREAD = 0;
    public static final int MESSAGE_READ = 1;

    public static void update(Context context, Uri uri, int status) {
        ContentValues updateValues = new ContentValues();
        updateValues.put("msg_box", status);
        context.getContentResolver().update(uri, updateValues, null, null);
    }

    public static void update(Context context, Uri uri, int status, String message_id) {
        ContentValues updateValues = new ContentValues();
        updateValues.put("msg_box", status);
        updateValues.put("m_id", message_id);
        context.getContentResolver().update(uri, updateValues, null, null);
    }

    public static void markUnread(Context context, String phoneNumber) {
        Cursor cursor = context.getContentResolver()
                .query(Uri.parse("content://mms/inbox"), null, null, null, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                if ((cursor.getString(cursor.getColumnIndex("address")).equals(phoneNumber))) {
                    String messageId = cursor.getString(cursor.getColumnIndex("_id"));
                    ContentValues updateValues = new ContentValues();
                    updateValues.put("read", MESSAGE_UNREAD);
                    context.getContentResolver()
                            .update(Uri.parse("content://mms/" + messageId), updateValues, null,
                                    null);
                }
            }

            cursor.close();
        }
    }

    public static void markRead(Context context, String phoneNumber) {
        Cursor cursor = context.getContentResolver()
                .query(Uri.parse("content://mms/inbox"), null, null, null, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                if (cursor.getString(cursor.getColumnIndex("address")).equals(phoneNumber)) {
                    String messageId = cursor.getString(cursor.getColumnIndex("_id"));
                    ContentValues updateValues = new ContentValues();
                    updateValues.put("read", MESSAGE_READ);
                    context.getContentResolver()
                            .update(Uri.parse("content://mms/" + messageId), updateValues, null,
                                    null);
                }
            }

            cursor.close();
        }
    }

    /**
     * @param context
     * @param msg_box    - {@link #MESSAGE_BOX_SENT} is recommended for outgoing MMS
     * @param to
     * @param subject
     * @param imageByte
     * @param timeOfText
     * @return
     */
    public static Uri insertSentMms(Context context, int msg_box, String[] to, String subject,
                                    byte[] imageByte, long timeOfText) {
        Log.d(TAG, "insertSentMms() called with: " + "context = [" + context + "], msg_box = [" +
                msg_box + "], " +
                "to = [" + Arrays.toString(to) + "], subject = [" + subject + "], imageByte = [" +
                Arrays.toString(imageByte) + "], " +
                "timeOfText = [" + timeOfText + "]");

        try {
            // Get thread id
            Set<String> recipients = new HashSet<>();
            recipients.addAll(Arrays.asList(to));
            long thread_id = getOrCreateThreadId(context, recipients);
            Log.d(TAG, "[DATABASE] Thread ID is " + thread_id);

            // Create a new message entry
            ContentValues mmsValues = new ContentValues();
            mmsValues.put(Telephony.Mms.THREAD_ID, thread_id);
            mmsValues.put(Telephony.Mms.DATE, timeOfText);
            mmsValues.put(Telephony.Mms.DATE_SENT, timeOfText);
            mmsValues.put(Telephony.Mms.MESSAGE_BOX, msg_box);
            mmsValues.put(Telephony.Mms.READ, MESSAGE_READ);
            mmsValues.put(Telephony.Mms.SUBJECT, subject);
            mmsValues.put(Telephony.Mms.CONTENT_TYPE, "application/vnd.wap.multipart.related");
            mmsValues.put(Telephony.Mms.EXPIRY, 604800);
            mmsValues.put(Telephony.Mms.MESSAGE_CLASS, "personal");
            mmsValues.put(Telephony.Mms.MESSAGE_TYPE, MessageConstants.MMS_TYPE_SENT);
            mmsValues.put(Telephony.Mms.MMS_VERSION, 18);
            mmsValues.put(Telephony.Mms.PRIORITY, 129);
            mmsValues.put(Telephony.Mms.READ_REPORT, 129);
            mmsValues.put(Telephony.Mms.TRANSACTION_ID, "T" + Long.toHexString(timeOfText));
            mmsValues.put(Telephony.Mms.DELIVERY_REPORT, 129);
            mmsValues.put(Telephony.Mms.RESPONSE_STATUS, 128);

            // Check if column exists (Fix for Xperia)
            Cursor c = context.getContentResolver()
                    .query(Telephony.Mms.CONTENT_URI, null, null, null, null);
            if (c != null && c.getColumnIndex("sequence_time") != -1) {
                mmsValues.put("sequence_time", timeOfText);

                c.close();
            }

            // Insert message
            Uri res = context.getContentResolver().insert(Telephony.Mms.CONTENT_URI, mmsValues);

            if (res != null) {
                String messageId = res.getLastPathSegment().trim();
                Log.d(TAG, "[DATABASE] Message saved as " + res);

                // Create parts
                createPart(context, messageId, imageByte);
//                for (byte[] imageByte : imageBytes) {
//                    createPart(context, messageId, imageByte);
//                }

                // Create addresses
                for (String addr : to) {
                    createAddr(context, messageId, addr);
                }
            }

            return res;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * @param context
     * @param msg_box    - {@link #MESSAGE_BOX_INBOX} is recommended for incoming MMS
     * @param to
     * @param subject
     * @param imageByte
     * @param timeOfText
     * @return
     */
    public static Uri insertIncomingMms(Context context, int msg_box, String[] to, String subject,
                                        byte[] imageByte, long timeOfText) {
        Log.d(TAG,
                "insertIncomingMms() called with: " + "context = [" + context + "], msg_box = [" +
                        msg_box + "], " +
                        "to = [" + Arrays.toString(to) + "], subject = [" + subject + "], " +
                        "imageByte = [" + Arrays.toString(imageByte) + "], timeOfText = [" +
                        timeOfText + "]");

        try {
            // Get thread id
            Set<String> recipients = new HashSet<>();
            recipients.addAll(Arrays.asList(to));
            long thread_id = getOrCreateThreadId(context, recipients);
            Log.d(TAG, "[DATABASE] Thread ID is " + thread_id);

            // Create a new message entry
            ContentValues mmsValues = new ContentValues();
            mmsValues.put(Telephony.Mms.THREAD_ID, thread_id);
            mmsValues.put(Telephony.Mms.DATE, timeOfText);
            mmsValues.put(Telephony.Mms.DATE_SENT, timeOfText);
            mmsValues.put(Telephony.Mms.MESSAGE_BOX, msg_box);
            mmsValues.put(Telephony.Mms.READ, MESSAGE_READ);
            mmsValues.put(Telephony.Mms.SUBJECT, subject);
            mmsValues.put(Telephony.Mms.CONTENT_TYPE, "application/vnd.wap.multipart.related");
            mmsValues.put(Telephony.Mms.EXPIRY, 604800);
            mmsValues.put(Telephony.Mms.MESSAGE_CLASS, "personal");
            mmsValues.put(Telephony.Mms.MESSAGE_TYPE, MessageConstants.MMS_TYPE_SENT);
            mmsValues.put(Telephony.Mms.MMS_VERSION, 18);
            mmsValues.put(Telephony.Mms.PRIORITY, 129);
            mmsValues.put(Telephony.Mms.READ_REPORT, 129);
            mmsValues.put(Telephony.Mms.TRANSACTION_ID, "T" + Long.toHexString(timeOfText));
            mmsValues.put(Telephony.Mms.DELIVERY_REPORT, 129);
            mmsValues.put(Telephony.Mms.RESPONSE_STATUS, 128);

            // Check if column exists (Fix for Xperia)
            Cursor c = context.getContentResolver()
                    .query(Telephony.Mms.CONTENT_URI, null, null, null, null);
            if (c != null && c.getColumnIndex("sequence_time") != -1) {
                mmsValues.put("sequence_time", timeOfText);
                c.close();
            }

            // Insert message
            Uri res = context.getContentResolver().insert(Telephony.Mms.CONTENT_URI, mmsValues);

            if (res != null) {
                String messageId = res.getLastPathSegment().trim();
                Log.d(TAG, "[DATABASE] Message saved as " + res);

                // Create parts
                createPart(context, messageId, imageByte);
//                for (byte[] imageByte : imageBytes) {
//                    createPart(context, messageId, imageByte);
//                }

                // Create addresses
                for (String addr : to) {
                    createAddr(context, messageId, addr);
                }
            }

            return res;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private static Uri createPart(Context context, String id, byte[] imageBytes) throws Exception {
        long now = System.currentTimeMillis();
        ContentValues mmsPartValue = new ContentValues();
        mmsPartValue.put("mid", id);
        mmsPartValue.put("ct", "image/jpeg");
        mmsPartValue.put("name", now + ".jpg");
        mmsPartValue.put("cl", now + ".jpg");
        mmsPartValue.put("cid", "<" + now + ".jpg>");
        Uri partUri = Uri.parse("content://mms/" + id + "/part");
        Uri res = context.getContentResolver().insert(partUri, mmsPartValue);
        if (res != null) {
            Log.d(TAG, "[DATABASE] Part uri is " + res.toString());

            // Add data to part
            OutputStream os = context.getContentResolver().openOutputStream(res);
            ByteArrayInputStream is = new ByteArrayInputStream(imageBytes);
            byte[] buffer = new byte[256];
            for (int len = 0; (len = is.read(buffer)) != -1; ) {
                if (os != null) {
                    os.write(buffer, 0, len);
                }
            }
            if (os != null) {
                os.close();
            }
            is.close();
        }

        return res;
    }

    private static Uri createAddr(Context context, String id, String addr) throws Exception {
        ContentValues addrValues = new ContentValues();
        addrValues.put("address", addr);
        addrValues.put("charset", "106");
        addrValues.put("type", MessageConstants.MMS_TYPE_ADDR_RECIPIENT); // TO
        Uri addrUri = Uri.parse("content://mms/" + id + "/addr");
        Uri res = context.getContentResolver().insert(addrUri, addrValues);

        if (res != null) {
            Log.d(TAG, "createAddr() returned: " + res.toString());
        }

        return res;
    }

    /**
     * This is a single-recipient version of getOrCreateThreadId.  It's convenient for use with SMS
     * messages.
     */
    public static long getOrCreateThreadId(Context context, String recipient) {
        Set<String> recipients = new HashSet<String>();
        recipients.add(recipient);
        return getOrCreateThreadId(context, recipients);
    }

    /**
     * Given the recipients list and subject of an unsaved message, return its thread ID.  If the
     * message starts a new thread, allocate a new thread ID.  Otherwise, use the appropriate
     * existing thread ID.
     * <p/>
     * Find the thread ID of the same set of recipients (in any order, without any additions). If
     * one is found, return it.  Otherwise, return a unique thread ID.
     */
    public static long getOrCreateThreadId(Context context, Set<String> recipients) {
        Uri.Builder uriBuilder = Uri.parse("content://mms-sms/threadID").buildUpon();

        for (String recipient : recipients) {
            uriBuilder.appendQueryParameter("recipient", recipient);
        }

        Uri uri = uriBuilder.build();
        Cursor cursor = context.getContentResolver()
                .query(uri, new String[] {"_id"}, null, null, null);
        if (cursor != null) {
            try {
                if (cursor.moveToFirst()) {
                    return cursor.getLong(0);
                } else {
                    Log.e(TAG, "getOrCreateThreadId returned no rows!");
                }
            } finally {
                cursor.close();
            }
        }
        return -1;
    }
}