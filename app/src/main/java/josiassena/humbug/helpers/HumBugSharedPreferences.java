package josiassena.humbug.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.annotation.ColorInt;

/**
 * File created by josias on 12/30/15.
 */
public class HumBugSharedPreferences {

    public static final String CONVERSATION_URI = "content://mms-sms/conversations/?simple=true";
    public static final int NEW_MESSAGE_NOTIFICATION_ID = 43561;
    public static final int NEW_MESSAGE_NOTIFICATION_REQUEST_CODE = 54243;
    public static final String CONVO_TO_MARK_READ = "markconvoread";
    public static final int START_NEW_CONVERSATION = 1524;
    public static final String FAKE_SMS = "fakeSms";
    public static final String GO_TO_TEXT_MSG_ACTIVITY = "gototxtmsgactivity";
    public static final String INBOX_URI = "content://sms/";
    public static final String COMING_FROM_NOTIFICATION = "coming_from_notification";

    public static final int MARKING_CONVO_READ = 963;
    public static final int MESSAGE_IS_NOT_READ = 0;
    public static final int MESSAGE_IS_NOT_SEEN = 0;
    public static final int MESSAGE_IS_READ = 1;
    public static final int MESSAGE_IS_SEEN = 1;

    private static final String NOTIFICATION_DISMISSED = "notification_dismissed";
    private static final String SENDING_FAKE_SMS = "sending_fake_sms";
    private static final String SHARED_PREFS_LABEL = "fake_sms_shared_prefs";
    public static final String UNREAD_MSG_COUNT = "unread_count";
    public static final String NOTIFICATION_PHONE_NUMBER = "notification_phone_number";
    public static final String STARTING_NEW_CONVERSATION = "creatingnewconvo";
    public static final String ACTION_MMS_SENT = "com.example.android.apis.os.MMS_SENT_ACTION";
    private static final String SMS_MMS_CANONICAL_ADDRESS = "content://mms-sms/canonical-address/";
    public static final String MMS_RECEIVED_WITHOUT_IMPL = "mms_received_without_mms_implementation";
    private static final String TOOLBAR_BACKGROUND_COLOR = "toolbarbgcolor";
    private static final String TOOLBAR_TEXT_COLOR = "toolbartextcolor";

    private static Context context;

    private static SharedPreferences getSharedPrefs() {
        return context.getSharedPreferences("humbug_sms_shared_prefs", 0);
    }

    private static SharedPreferences.Editor getSharedPrefsEditor() {
        return getSharedPrefs().edit();
    }

    public static boolean isNotificationDismissed() {
        return getSharedPrefs().getBoolean("notification_dismissed", false);
    }

    public static void setContext(Context context) {
        HumBugSharedPreferences.context = context;
    }

    public static void setNotificationDismissed(boolean notificationDismissed) {
        getSharedPrefsEditor().putBoolean("notification_dismissed", notificationDismissed).commit();
    }

    public static void setToolBarBackGroundColor(@ColorInt int toolbarBackgroundColor) {
        getSharedPrefsEditor().putInt(TOOLBAR_BACKGROUND_COLOR, toolbarBackgroundColor).commit();
    }

    public static int getToolBarBackGroundColor() {
        return getSharedPrefs().getInt(TOOLBAR_BACKGROUND_COLOR, Color.parseColor("#ff2196f3"));
    }

    public static void setToolBarTextColor(@ColorInt int toolBarTextColor) {
        getSharedPrefsEditor().putInt(TOOLBAR_TEXT_COLOR, toolBarTextColor).commit();
    }

    public static int getToolBarTextColor() {
        return getSharedPrefs().getInt(TOOLBAR_TEXT_COLOR, Color.WHITE);
    }

    /**
     * Determins whether or not the app has enabled MMS over WiFi
     *
     * @param context
     * @return true if enabled
     */
    public static boolean isMmsOverWifiEnabled(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean("mms_over_wifi", false);
    }
}
