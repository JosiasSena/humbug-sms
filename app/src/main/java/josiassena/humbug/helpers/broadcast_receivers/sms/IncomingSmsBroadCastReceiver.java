package josiassena.humbug.helpers.broadcast_receivers.sms;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.provider.Telephony;
import android.support.annotation.NonNull;
import android.telephony.SmsMessage;
import android.util.Log;

import java.util.ArrayList;

import josiassena.humbug.helpers.Utils;
import josiassena.humbug.helpers.callbacks.NewMessageReceivedListener;
import josiassena.humbug.msg.Message;
import josiassena.humbug.msg.MessageType;
import josiassena.humbug.notification.SmsNotification;

import static josiassena.humbug.helpers.HumBugSharedPreferences.MESSAGE_IS_NOT_READ;
import static josiassena.humbug.helpers.HumBugSharedPreferences.MESSAGE_IS_NOT_SEEN;

/**
 * Called when a we have received an SMS
 * <p>
 * File created by josias on 12/30/15.
 */
public class IncomingSmsBroadCastReceiver extends BroadcastReceiver {

    private static final String TAG = IncomingSmsBroadCastReceiver.class.getSimpleName();
    private static final ArrayList<NewMessageReceivedListener> callbackList = new ArrayList<>();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive() was called");

        Utils.wakeUpPhone(context, TAG);

        final SmsMessage[] smsMessages = Telephony.Sms.Intents.getMessagesFromIntent(intent);
        final ArrayList<Message> smsArrayList = new ArrayList<>();

        if (smsMessages != null && smsMessages.length > 0) {
            for (SmsMessage smsMessage : smsMessages) {
                insertSMSIntoLocalDatabase(context, smsMessage);

                if (Utils.getSharedPreferenceBoolean(context, "notifications_new_message")) {
                    new SmsNotification(context, smsMessage, getTotalUnreadMessages(context))
                            .display();
                }

                smsArrayList.add(getMessage(smsMessage));
            }
        }

        for (NewMessageReceivedListener callback : callbackList) {
            callback.onNewMessagesReceived(smsArrayList);
        }
    }

    @NonNull
    private Message getMessage(final SmsMessage smsMessage) {
        return new Message.Builder()
                .setMessageType(MessageType.THEM)
                .setPhoneNumber(smsMessage.getOriginatingAddress())
                .setMessage(smsMessage.getMessageBody())
                .setTimeOfText(Utils.getCurrentTimeMs())
                .build();
    }

    public static void addNewMessageCallBack(final NewMessageReceivedListener listener) {
        callbackList.add(listener);
    }

    private int getTotalUnreadMessages(Context context) {
        final Cursor cursor = context.getContentResolver()
                .query(Telephony.Sms.Inbox.CONTENT_URI, null, "read = 0", null, null);

        int unreadCount = 0;
        if (cursor != null) {
            unreadCount = cursor.getCount();
            cursor.close();
        }

        return unreadCount;
    }

    private void insertSMSIntoLocalDatabase(final Context context,
                                            final SmsMessage smsMessage) {

        final ContentValues contentValues = new ContentValues();
        contentValues.put("address", smsMessage.getOriginatingAddress());
        contentValues.put("date", smsMessage.getTimestampMillis());
        contentValues.put("read", MESSAGE_IS_NOT_READ);
        contentValues.put("status", smsMessage.getStatus());
        contentValues.put("type", MessageType.THEM.getValue());
        contentValues.put("seen", MESSAGE_IS_NOT_SEEN);
        contentValues.put("body", smsMessage.getMessageBody());

        context.getContentResolver().insert(Telephony.Sms.CONTENT_URI, contentValues);
    }
}
