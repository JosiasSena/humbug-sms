package josiassena.humbug.helpers.broadcast_receivers.mms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Called when a MMS message has been received successfully by the other party
 *
 * File created by josias on 1/3/16.
 */
public class MmsReceivedReceiver extends BroadcastReceiver {

    private static final String TAG = "MmsReceivedReceiver";

    public static final String MMS_RECEIVED = "com.klinker.android.messaging.MMS_RECEIVED";
    public static final String EXTRA_FILE_PATH = "file_path";
    public static final String EXTRA_LOCATION_URL = "location_url";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.v(TAG, "MMS has finished downloading, persisting it to the database");

        String path = intent.getStringExtra(EXTRA_FILE_PATH);
        Log.v(TAG, path);

        try {
            File mDownloadFile = new File(path);
            final int nBytes = (int) mDownloadFile.length();
            FileInputStream reader = new FileInputStream(mDownloadFile);
            final byte[] response = new byte[nBytes];
            reader.read(response, 0, nBytes);

            // FIXME: 1/3/16
//            Uri uri = DownloadRequest.persist(context, response,
//                    new MmsConfig.Overridden(new MmsConfig(context), null),
//                    intent.getStringExtra(EXTRA_LOCATION_URL),
//                    Utils.getDefaultSubscriptionId(), null);

            //workaroundForFi(context, uri);

            Log.v(TAG, "response saved successfully");
            Log.v(TAG, "response length: " + response.length);
            mDownloadFile.delete();
        } catch (FileNotFoundException e) {
            Log.e(TAG, "MMS received, file not found exception", e);
        } catch (IOException e) {
            Log.e(TAG, "MMS received, io exception", e);
        }
    }
}