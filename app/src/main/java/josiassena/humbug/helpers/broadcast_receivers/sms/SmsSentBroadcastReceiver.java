package josiassena.humbug.helpers.broadcast_receivers.sms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

import josiassena.humbug.helpers.callbacks.NewMessageSentListener;

/**
 * Called when an SMS message has been sent successfully to the other party
 *
 * File created by josias on 12/30/15.
 */
public class SmsSentBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = SmsSentBroadcastReceiver.class.getSimpleName();
    private static ArrayList<NewMessageSentListener> callbackList = new ArrayList<>();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive() was called");

        Toast.makeText(context, "Message sent successfully", Toast.LENGTH_SHORT).show();

        for (NewMessageSentListener callback : callbackList) {
            callback.onNewMessageSent();
        }
    }

    public static void addNewMessageSentCallBack(NewMessageSentListener newMessageSentListener) {
        callbackList.add(newMessageSentListener);
    }
}
