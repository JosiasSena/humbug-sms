package josiassena.humbug.helpers.broadcast_receivers.mms;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.Telephony;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.MessageFormat;
import java.util.ArrayList;

import josiassena.humbug.R;
import josiassena.humbug.helpers.callbacks.NewMessageReceivedListener;
import josiassena.humbug.msg.Message;
import josiassena.humbug.msg.MessageConstants;

/**
 * File created by josias on 12/30/15.
 */
public class IncomingMmsBroadCastReceiver extends BroadcastReceiver {

    private static final String TAG = IncomingMmsBroadCastReceiver.class.getSimpleName();

    private static final String ACTION_MMS_RECEIVED = "android.provider.Telephony.WAP_PUSH_RECEIVED";
    private static final String ACTION_MMS_DELIVER = "android.provider.Telephony.WAP_PUSH_DELIVER";
    private static final String MMS_DATA_TYPE = "application/vnd.wap.mms-message";

    private static ArrayList<NewMessageReceivedListener> callbackList = new ArrayList<>();
    private ArrayList<Message> mmsArrayList = new ArrayList<>();

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        String type = intent.getType();

        if ((action.equals(ACTION_MMS_RECEIVED) || action.equals(ACTION_MMS_DELIVER)) &&
                type.equals(MMS_DATA_TYPE)) {
            // We received an MMS

            showReceivedAnMMSDialog(context);
            if (josiassena.humbug.helpers.Utils
                    .getSharedPreferenceBoolean(context, "notifications_new_message")) {
                new josiassena.humbug.notification.MmsNotification(context,
                        getTotalUnreadMessages(context));
            }

            // TODO: 1/22/16 add MMS functionality to HumBug
//            try {
//                Bundle bundle = intent.getExtras();
//                if (bundle != null) {
//                    showReceivedAnMMSDialog(context);
//                    byte[] dataBytes = bundle.getByteArray("data");
//                    if (dataBytes != null) {
//                        Cursor cursor = context.getContentResolver().query(Message.MMS_INBOX_CONTENT_URI, null,
//                                "m_type in (" + Message.MMS_TYPE_RECEIVED + "," + Message.MMS_TYPE_SENT + ")", null, null);
//
//                        Cursor a = context.getContentResolver().query(Message.MMS_CONTENT_URI, null,
//                                "m_type in (" + Message.MMS_TYPE_RECEIVED + "," + Message.MMS_TYPE_SENT + ")", null, null);
//
//                        Cursor b = context.getContentResolver().query(Message.MMS_INBOX_CONTENT_URI, null,
//                                "m_type in (" + Message.MMS_TYPE_RECEIVED + "," + Message.MMS_TYPE_SENT + ")", null, null);
//
//                        Utils.logCursorColumns(TAG, a);
//                        Utils.logCursorColumns(TAG, b);
//
//                        if (cursor != null) {
//                            Utils.logCursorColumns(TAG, cursor);
//                            String buffer = new String(dataBytes);
//                            try {
//                                if (cursor.getCount() > 0) {
//                                    cursor.moveToFirst();
//                                    do {
//                                        int id = Utils.getInt(cursor, "_id");
//
//                                        // if buffer contains 'id' then this means that this is the message that was just received
//                                        if (buffer.contains(String.valueOf(id))) {
//                                            Message message = new Message();
//                                            message.setFake(false);
//                                            message.setIsMms(true);
//                                            message.setTimeOfText(GregorianCalendar.getInstance().getTimeInMillis());
//                                            message.setMessageType(Message.THEM);
//                                            fillMessage(context, id, message); // adds message to message object
//                                            retrieveAddress(context, message, id); // adds phone number to message
//
//                                            mmsArrayList.add(message);
//                                            putMmsToDatabase(context, message);
//                                            new MmsNotification(context, message, getTotalUnreadMessages(context));
//                                        }
//
//                                    } while (cursor.moveToNext());
//                                }
//                            } finally {
//                                cursor.close();
//                            }
//                        }
//                    }
//                }
//            } catch (Exception e) {
//                Log.e(TAG, e.getMessage(), e);
//            }
        }

        for (josiassena.humbug.helpers.callbacks.NewMessageReceivedListener callback : callbackList) {
            callback.onNewMessagesReceived(mmsArrayList);
        }
    }

    private void showReceivedAnMMSDialog(Context context) {
        new AlertDialog.Builder(context)
                .setTitle(R.string.received_mms)
                .setMessage(
                        R.string.sms_not_supported)
                .setPositiveButton(R.string.word_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
    }

    private void putMmsToDatabase(Context context, Message message) {
        Bitmap[] images = message.getImages();

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        images[0].compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteArray = stream.toByteArray();

        josiassena.humbug.helpers.database.MmsDatabase.insertSentMms(context,
                josiassena.humbug.helpers.database.MmsDatabase.MESSAGE_BOX_INBOX,
                new String[] {message.getPhoneNumber()},
                "You received a picture.",
                byteArray, message.getTimeOfText());
    }

    public static void addNewMessageCallBack(
            josiassena.humbug.helpers.callbacks.NewMessageReceivedListener newMessageReceivedListener) {
        callbackList.add(newMessageReceivedListener);
    }

    private int getTotalUnreadMessages(Context context) {
        Cursor cursor = context.getContentResolver()
                .query(Telephony.Mms.Inbox.CONTENT_URI, null, "read = 0", null, null);

        int unreadCount = 0;
        if (cursor != null) {
            unreadCount = cursor.getCount();
            cursor.close();
        }

        Log.d(TAG, "getTotalUnreadMessages() returned: " + unreadCount);
        return unreadCount;
    }

    private void fillMessage(Context context, int mmsPartId, Message message) {
        // Read the content of the MMS
        Cursor cursor = context.getContentResolver()
                .query(MessageConstants.MMS_PART_CONTENT_URI, null, "mid = " + mmsPartId, null,
                        null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                String partId = josiassena.humbug.helpers.Utils.getString(cursor, "_id");
                String type = josiassena.humbug.helpers.Utils.getString(cursor, "ct");
                if ("text/plain".equals(type)) {
                    String data = josiassena.humbug.helpers.Utils.getString(cursor, "_data");
                    if (data != null) {
                        message.setMessage(getMessageText(context, partId));
                    } else {
                        message.setMessage(
                                josiassena.humbug.helpers.Utils.getString(cursor, "text"));
                    }
                } else if ("image/jpeg".equals(type) || "image/bmp".equals(type) ||
                        "image/gif".equals(type) || "image/jpg".equals(type) ||
                        "image/png".equals(type)) {
                    message.setImage(josiassena.humbug.helpers.Utils.getMmsImage(context, partId));
                }
            } while (cursor.moveToNext());

            cursor.close();
        }
    }

    private String getMessageText(Context context, String mmsPartId) {
        InputStream inputStream = null;
        StringBuilder stringBuilder = new StringBuilder();
        try {
            inputStream = context.getContentResolver()
                    .openInputStream(
                            Uri.withAppendedPath(MessageConstants.MMS_PART_CONTENT_URI, mmsPartId));
            if (inputStream != null) {
                InputStreamReader isr = new InputStreamReader(inputStream, "UTF-8");
                BufferedReader reader = new BufferedReader(isr);
                String temp = reader.readLine();
                while (temp != null) {
                    stringBuilder.append(temp);
                    temp = reader.readLine();
                }
            }
        } catch (IOException e) {
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
            }
        }
        return stringBuilder.toString();
    }

    private void retrieveAddress(Context context, Message message, int id) {
        String selectionAdd = "msg_id = " + id + " and type = " +
                MessageConstants.MMS_TYPE_ADDR_SENDER;
        String uriStr = MessageFormat.format("content://mms/{0}/addr", id);
        Uri uriAddress = Uri.parse(uriStr);
        Cursor cursor = context.getContentResolver()
                .query(uriAddress, null, selectionAdd, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            do {
                String address = josiassena.humbug.helpers.Utils.getString(cursor, "address");
                if (!address.equals("insert-address-token")) {
                    message.setPhoneNumber(address);
                }
            } while (cursor.moveToNext());

            cursor.close();
        }
    }

    private static Uri createPart(Context context, String id, byte[] imageBytes) throws Exception {
        ContentValues mmsPartValue = new ContentValues();
        mmsPartValue.put("mid", id);
        mmsPartValue.put("ct", "image/png");
        mmsPartValue.put("cid", "<" + System.currentTimeMillis() + ">");
        Uri partUri = Uri.parse("content://mms/" + id + "/part");
        Uri res = context.getContentResolver().insert(partUri, mmsPartValue);

        // Add data to part
        OutputStream os = null;
        if (res != null) {
            os = context.getContentResolver().openOutputStream(res);
        }
        ByteArrayInputStream is = new ByteArrayInputStream(imageBytes);
        byte[] buffer = new byte[256];
        for (int len = 0; (len = is.read(buffer)) != -1; ) {
            if (os != null) {
                os.write(buffer, 0, len);
            }
        }
        if (os != null) {
            os.close();
        }
        is.close();

        return res;
    }

    private static Uri createAddr(Context context, String id, String addr) throws Exception {
        ContentValues addrValues = new ContentValues();
        addrValues.put("address", addr);
        addrValues.put("charset", "106");
        addrValues.put("type", MessageConstants.MMS_TYPE_ADDR_RECIPIENT); // TO
        Uri addrUri = Uri.parse("content://mms/" + id + "/addr");
        return context.getContentResolver().insert(addrUri, addrValues);
    }

}
