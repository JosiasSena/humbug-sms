package josiassena.humbug.helpers.broadcast_receivers.mms;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.Telephony;
import android.util.Log;

import java.io.File;

/**
 * Called when a MMS message has been sent successfully to the other party
 *
 * File created by josias on 1/3/16.
 */
public class MmsSentReceiver extends BroadcastReceiver {

    private static final String TAG = "MmsSentReceiver";
    public static final String MMS_SENT = "com.klinker.android.messaging.MMS_SENT";
    public static final String EXTRA_CONTENT_URI = "content_uri";
    public static final String EXTRA_FILE_PATH = "file_path";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.v(TAG, "MMS has finished sending, marking it as so in the database");

        Uri uri = Uri.parse(intent.getStringExtra(EXTRA_CONTENT_URI));
        Log.v(TAG, uri.toString());

        ContentValues values = new ContentValues(1);
        values.put(Telephony.Mms.MESSAGE_BOX, Telephony.Mms.MESSAGE_BOX_SENT);
//        SqliteWrapper.update(context, context.getContentResolver(), uri, values,
//                null, null);

        String filePath = intent.getStringExtra(EXTRA_FILE_PATH);
        Log.v(TAG, filePath);
        new File(filePath).delete();
    }
}
