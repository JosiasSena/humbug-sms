package josiassena.humbug.helpers.broadcast_receivers.sms;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Called when an SMS message has been delivered successfully to the other party
 * <p>
 * File created by josias on 12/30/15.
 */
public class SmsDeliveredBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = SmsDeliveredBroadcastReceiver.class.getSimpleName();

    @SuppressLint ("LongLogTag")
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "The message has been successfully delivered.");
        // TODO: 12/21/16 this can be handled to notify the user if the
        // message was actually received by the other person after it was sent.
    }
}
