package josiassena.humbug.helpers;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.provider.Telephony;
import android.support.annotation.NonNull;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.UUID;

import josiassena.humbug.R;
import josiassena.humbug.helpers.broadcast_receivers.sms.SmsDeliveredBroadcastReceiver;
import josiassena.humbug.helpers.broadcast_receivers.sms.SmsSentBroadcastReceiver;
import josiassena.humbug.main.view.MainActivity;

/**
 * File created by josias on 12/30/15.
 */
public final class Utils {

    private static final String TAG = Utils.class.getSimpleName();

    private Utils() {
        // do nothing
    }

    public static String getContactId(Context context, String phoneNumber) {
        Log.d(TAG, "getContactId() called with: context = [" + context + "], phoneNumber = [" +
                phoneNumber + "]");
        Cursor cursor = context.getContentResolver()
                .query(Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                        Uri.encode(phoneNumber)), new String[] {ContactsContract.Contacts._ID},
                        null, null, null);

        String id = "";

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
            }

            cursor.close();
        }

        Log.d(TAG, "getContactId() returned: " + id);
        return id;
    }

    public static String getContactName(Context context, String phoneNumber) {
        Log.d(TAG, "getContactName() called with: context = [" + context + "], phoneNumber = [" +
                phoneNumber + "]");
        Cursor cursor = context.getContentResolver()
                .query(Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                        Uri.encode(phoneNumber)),
                        new String[] {ContactsContract.Contacts.DISPLAY_NAME}, null, null, null);

        String contactName = "";

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                contactName = cursor
                        .getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            }

            cursor.close();
        }

        Log.d(TAG, "getContactName() returned: " + contactName);
        return contactName;
    }

    public static String getContactPhoneNumber(Context context, int recipientId) {
        Log.d(TAG, "getContactPhoneNumber() called with: context = [" + context +
                "], recipientId = [" + recipientId + "]");
        Cursor cursor = context.getContentResolver()
                .query(Uri.parse("content://mms-sms/canonical-address/" + recipientId), null, null,
                        null, null);
        String phoneNumber = "";

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                phoneNumber = cursor.getString(cursor.getColumnIndex("address"));
            }
            cursor.close();
        }

        Log.d(TAG, "getContactPhoneNumber() returned: " + phoneNumber);
        return phoneNumber;
    }

    /**
     * Gets the current users phone number
     *
     * @param context is the context of the activity or service
     * @return a string of the phone number on the device
     */
    public static String getMyPhoneNumber(@NonNull Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getLine1Number();
    }

    public static void markMessagesRead(Context context, String number) {
        Log.d(TAG,
                "markMessagesRead() called with: context = [" + context + "], number = [" + number +
                        "]");
        Cursor cursor = context.getContentResolver()
                .query(Telephony.Sms.Inbox.CONTENT_URI, null, null, null, null);

        if (cursor != null) {
            while (cursor.moveToNext()) {
                if (cursor.getString(cursor.getColumnIndex("address")).equals(number) &&
                        cursor.getInt(cursor.getColumnIndex("read")) == 0) {
                    String str = cursor.getString(cursor.getColumnIndex("_id"));
                    ContentValues localContentValues = new ContentValues();
                    localContentValues.put("read", HumBugSharedPreferences.MESSAGE_IS_READ);
                    context.getContentResolver()
                            .update(Telephony.Sms.Inbox.CONTENT_URI, localContentValues,
                                    "_id=" + str, null);
                }
            }

            cursor.close();
        }
    }

    public static String getMessageId(Context context, String phoneNumber) {
        Log.d(TAG, "getContactName() called with: context = [" + context + "], phoneNumber = [" +
                phoneNumber + "]");
        Cursor cursor = context.getContentResolver()
                .query(Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                        Uri.encode(phoneNumber)), new String[] {ContactsContract.Contacts._ID},
                        null, null, null);

        String id = "";

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
            }

            cursor.close();
        }

        Log.d(TAG, "getContactId() returned: " + id);
        return id;
    }

    @SuppressLint ({"LongLogTag"})
    private static void putSentSmsToDatabase(Context context, String phoneNumber, String message,
                                             long timeSent) {
        Log.d(TAG, "putSentSmsToDatabase() called with: " + "context = [" + context +
                "], phoneNumber = [" + phoneNumber + "], message = [" + message +
                "], timeSent = [" + timeSent + "]");
        ContentValues contentValues = new ContentValues();
        contentValues.put(Telephony.Sms.Sent.ADDRESS, phoneNumber);
        contentValues.put(Telephony.Sms.Sent.DATE, timeSent);
        contentValues.put(Telephony.Sms.Sent.DATE_SENT, timeSent);
        contentValues.put(Telephony.Sms.Sent.BODY, message);

        Log.d(TAG, "putSentSmsToDatabase: " + contentValues.toString());
        context.getContentResolver().insert(Telephony.Sms.Sent.CONTENT_URI, contentValues);
    }

    public static void sendRealSms(Context context, String message, String phoneNumber,
                                   long timeSent) {
        Log.d(TAG, "sendRealSms() called with: context = [" + context + "], message = [" + message +
                "], phoneNumber = [" + phoneNumber + "]");

        PendingIntent messageSentPendingIntent = PendingIntent
                .getBroadcast(context, 0, new Intent(context, SmsSentBroadcastReceiver.class), 0);

        PendingIntent messageDeliveredPendingIntent = PendingIntent
                .getBroadcast(context, 0, new Intent(context, SmsDeliveredBroadcastReceiver.class),
                        0);

        try {
            SmsManager.getDefault()
                    .sendTextMessage(phoneNumber, null, message, messageSentPendingIntent,
                            messageDeliveredPendingIntent);
            putSentSmsToDatabase(context, phoneNumber, message, timeSent);
        } catch (Exception e) {
            Toast.makeText(context, "Sending message failed, please try again later.",
                    Toast.LENGTH_LONG).show();
            Log.e(TAG, e.getMessage(), e);
        }
    }

    public static void wakeUpPhone(@NonNull Context context, @NonNull String tag) {
        final PowerManager powerManager = (PowerManager) context
                .getSystemService(Context.POWER_SERVICE);

        final PowerManager.WakeLock wakeLock = powerManager.newWakeLock((
                PowerManager.SCREEN_BRIGHT_WAKE_LOCK |
                        PowerManager.FULL_WAKE_LOCK |
                        PowerManager.ACQUIRE_CAUSES_WAKEUP), tag);

        wakeLock.acquire();
    }

    public static String getContactPhoneNumberFromName(Context context, String name) {
        ContentResolver contentResolver = context.getContentResolver();
        Cursor cursor = contentResolver
                .query(ContactsContract.Contacts.CONTENT_URI, null, "display_name = '" + name + "'",
                        null, null);
        String number = "";

        if (cursor != null && cursor.moveToFirst()) {
            String contactId = cursor
                    .getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));

            //  Get all phone numbers.
            Cursor phones = contentResolver
                    .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId,
                            null, null);

            if (phones != null) {
                while (phones.moveToNext()) {
                    number = phones.getString(
                            phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                }

                phones.close();
            }

            cursor.close();
        }

        return number;
    }

    public static void hideSoftKeyboard(@NonNull Activity activity) {
        final InputMethodManager inputMethodManager = (InputMethodManager) activity
                .getSystemService(Activity.INPUT_METHOD_SERVICE);

        final View currentFocus = activity.getCurrentFocus();

        if (currentFocus != null) {
            inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
        }
    }

    /**
     * Checks whether or not mobile data is enabled and returns the result
     *
     * @param context is the context of the activity or service
     * @return true if data is enabled or false if disabled
     */
    public static Boolean isMobileDataEnabled(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        try {
            Class<?> c = Class.forName(cm.getClass().getName());
            Method m = c.getDeclaredMethod("getMobileDataEnabled");
            m.setAccessible(true);
            return (Boolean) m.invoke(cm);
        } catch (Exception e) {
            Log.e(TAG, "exception thrown", e);
            return null;
        }
    }

    /**
     * Checks mobile data enabled based on telephonymanager
     *
     * @param telephonyManager the telephony manager
     */
    public static boolean isDataEnabled(TelephonyManager telephonyManager) {
        try {
            Class<?> c = telephonyManager.getClass();
            Method m = c.getMethod("getDataEnabled");
            return (boolean) m.invoke(telephonyManager);
        } catch (Exception e) {
            Log.e(TAG, "exception thrown", e);
            return true;
        }
    }

    /**
     * Checks mobile data enabled based on telephonymanager and sim card
     *
     * @param telephonyManager the telephony manager
     * @param subId            the sim card id
     */
    public static boolean isDataEnabled(TelephonyManager telephonyManager, int subId) {
        try {
            Class<?> c = telephonyManager.getClass();
            Method m = c.getMethod("getDataEnabled", int.class);
            return (boolean) m.invoke(telephonyManager, subId);
        } catch (Exception e) {
            Log.e(TAG, "exception thrown", e);
            return isDataEnabled(telephonyManager);
        }
    }

    /**
     * Determines whether or not the user has Android 4.4 KitKat
     *
     * @return true if version code on device is >= kitkat
     */
    public static boolean hasKitKat() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
    }

    /**
     * Determines whether or not the app is the default SMS app on a device
     *
     * @param context
     * @return true if app is default
     */
    public static boolean isDefaultSmsApp(Context context) {
        return !hasKitKat() ||
                context.getPackageName().equals(Telephony.Sms.getDefaultSmsPackage(context));

    }

    public static Uri getImageUri(Context context, Bitmap bitmap) {
        Log.d(TAG,
                "getImageUri() called with: " + "context = [" + context + "], bitmap = [" + bitmap +
                        "]");

//        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//
//        if (bitmap != null) {
//            bitmap.compress(Bitmap.CompressFormat.JPEG, 0, bytes);
//        }

        String path = MediaStore.Images.Media
                .insertImage(context.getContentResolver(), bitmap, "HumBug SMS Image", null);
        return Uri.parse(path);
    }

    public static Bitmap getMmsImage(Context context, String mmsPartId) {
        Log.d(TAG, "getMmsImage() called with: " + "context = [" + context + "], mmsPartId = [" +
                mmsPartId + "]");

        Uri uri = Uri.parse("content://mms/part/" + mmsPartId);
        InputStream inputStream = null;
        Bitmap bitmap = null;
        try {
            inputStream = context.getContentResolver().openInputStream(uri);

            BitmapFactory.Options options = new BitmapFactory.Options();

            // First decode with inJustDecodeBounds = true to check dimensions
            options.inJustDecodeBounds = true;

            BitmapFactory.decodeStream(inputStream, new Rect(0, 0, 0, 0), options);

            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, 100, 100);

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            options.inPreferQualityOverSpeed = false;

            inputStream = context.getContentResolver().openInputStream(uri);
            bitmap = BitmapFactory.decodeStream(inputStream, new Rect(0, 0, 0, 0), options);

        } catch (IOException e) {
            Log.e(TAG, e.getMessage(), e);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    Log.e(TAG, e.getMessage(), e);
                }
            }
        }

        Log.d(TAG, "getMmsImage() returned (bitmap == null): " + (bitmap == null));
        return bitmap;
    }

    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth,
                                             int reqHeight) {
        Log.d(TAG, "calculateInSampleSize() called with: " + "options = [" + options +
                "], reqWidth = [" + reqWidth + "], reqHeight = [" + reqHeight + "]");

        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        Log.d(TAG, "calculateInSampleSize() returned: " + inSampleSize);
        return inSampleSize;
    }

    public static void logCursorColumns(String tag, Cursor cursor) {
        Log.e(tag, "logCursorColumns() called with " +
                "count = [" + cursor.getCount() + "] " +
                "columnCount = [" + cursor.getColumnCount() + "] " +
                "columns = [ " + Arrays.toString(cursor.getColumnNames()) + "]");
    }

    public static Long getLong(Cursor cursor, String col) {
        return cursor.getLong(cursor.getColumnIndex(col));
    }

    public static int getInt(Cursor cursor, String col) {
        return cursor.getInt(cursor.getColumnIndex(col));
    }

    public static String getString(Cursor cursor, String col) {
        return cursor.getString(cursor.getColumnIndex(col));
    }

    public static String getSharedAppName(Context context) {
        return PreferenceManager
                .getDefaultSharedPreferences(context)
                .getString("pref_app_name", "HumBug");
    }

    public static String getSharedPreferenceString(Context context, String key) {
        return PreferenceManager
                .getDefaultSharedPreferences(context)
                .getString(key, "");
    }

    public static boolean getSharedPreferenceBoolean(Context context, String key) {
        return PreferenceManager
                .getDefaultSharedPreferences(context)
                .getBoolean(key, true);
    }

    private static void addNewApplicationShortcut(Context context, String newAppName,
                                                  Bitmap newAppIcon) {
        // Intent to be send, when shortcut is pressed by user ("launched")
        Intent shortcutIntent = new Intent(context, MainActivity.class);

        // Decorate the shortcut
        Intent addIntent = new Intent();
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, newAppName);
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON, newAppIcon);

        // Inform launcher to create shortcut
        addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
        context.sendBroadcast(addIntent);
    }

    public static void changeAppShortcut(Context context, String name) {
        // Intent to be send, when shortcut is pressed by user ("launched")
        Intent shortcutIntent = new Intent(context, MainActivity.class);

        // Decorate the shortcut
        Intent delIntent = new Intent();
        delIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
        delIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, name);

        // Inform launcher to remove shortcut
        delIntent.setAction("com.android.launcher.action.UNINSTALL_SHORTCUT");
        context.sendBroadcast(delIntent);

        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.icon);
        addNewApplicationShortcut(context, name, bitmap);
    }

    public static Uri addNewContact(Context context, String phoneNumber, String contactName) {
        ContentValues values = new ContentValues();
        values.put(ContactsContract.Data.RAW_CONTACT_ID, getRandomId());
        values.put(ContactsContract.Data.MIMETYPE,
                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);
        values.put(ContactsContract.CommonDataKinds.Phone.NUMBER, phoneNumber);
        values.put(ContactsContract.CommonDataKinds.Phone.LABEL, contactName);
        values.put(ContactsContract.CommonDataKinds.Phone.TYPE,
                ContactsContract.CommonDataKinds.Phone.TYPE_CUSTOM);
        return context.getContentResolver().insert(ContactsContract.Data.CONTENT_URI, values);
    }

    public static String getRandomId() {
        return UUID.randomUUID().toString();
    }

    public static void openAddNewContactScreen(Activity activity, String phoneNumber,
                                               int requestCode) {
        Intent intent = new Intent(Intent.ACTION_INSERT_OR_EDIT);
        intent.setType(ContactsContract.Contacts.CONTENT_ITEM_TYPE);
        intent.putExtra(ContactsContract.Intents.Insert.PHONE, phoneNumber);
        activity.startActivityForResult(intent, requestCode);
    }

    public static void dismissMessageNotification(final Context context) {
        final NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.cancel(HumBugSharedPreferences.NEW_MESSAGE_NOTIFICATION_ID);
    }

    public static void copyTextToClipboard(@NonNull final Context context,
                                           @NonNull final String label,
                                           @NonNull final String text) {
        final ClipData clip = ClipData.newPlainText(label, text);
        final ClipboardManager clipboard = (ClipboardManager) context
                .getSystemService(Context.CLIPBOARD_SERVICE);

        clipboard.setPrimaryClip(clip);

        Toast.makeText(context, "Message copied to clipboard", Toast.LENGTH_SHORT).show();
    }

    public static long getCurrentTimeMs() {
        return GregorianCalendar.getInstance().getTimeInMillis();
    }

    public static void logCursorMessageInfo(@NonNull final String tag,
                                            @NonNull final Cursor cursor) {
        Log.d(tag, "logCursorMessageInfo _ID: " +
                cursor.getString(cursor.getColumnIndex(Telephony.Sms._ID)));

        Log.d(tag, "logCursorMessageInfo THREAD_ID: " +
                cursor.getString(cursor.getColumnIndex(Telephony.Sms.THREAD_ID)));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            Log.d(tag, "logCursorMessageInfo SUBSCRIPTION_ID: " +
                    cursor.getString(cursor.getColumnIndex(Telephony.Sms.SUBSCRIPTION_ID)));
        }

        Log.d(tag, "logCursorMessageInfo DATE: " +
                cursor.getString(cursor.getColumnIndex(Telephony.Sms.DATE)));

        Log.d(tag, "logCursorMessageInfo DATE_SENT: " +
                cursor.getString(cursor.getColumnIndex(Telephony.Sms.DATE_SENT)));

        Log.d(tag, "logCursorMessageInfo SUBJECT: " +
                cursor.getString(cursor.getColumnIndex(Telephony.Sms.SUBJECT)));

        Log.d(tag, "logCursorMessageInfo ADDRESS: " +
                cursor.getString(cursor.getColumnIndex(Telephony.Sms.ADDRESS)));

        Log.d(tag, "logCursorMessageInfo BODY: " +
                cursor.getString(cursor.getColumnIndex(Telephony.Sms.BODY)));

        Log.d(tag, "logCursorMessageInfo TYPE: " +
                cursor.getString(cursor.getColumnIndex(Telephony.Sms.TYPE)));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Log.d(tag, "logCursorMessageInfo CREATOR: " +
                    cursor.getString(cursor.getColumnIndex(Telephony.Sms.CREATOR)));
        }

        Log.d(tag, "logCursorMessageInfo ERROR_CODE: " +
                cursor.getString(cursor.getColumnIndex(Telephony.Sms.ERROR_CODE)));

        Log.d(tag, "logCursorMessageInfo LOCKED: " +
                cursor.getString(cursor.getColumnIndex(Telephony.Sms.LOCKED)));

        Log.d(tag, "logCursorMessageInfo PERSON: " +
                cursor.getString(cursor.getColumnIndex(Telephony.Sms.PERSON)));

        Log.d(tag, "logCursorMessageInfo READ: " +
                cursor.getString(cursor.getColumnIndex(Telephony.Sms.READ)));

        Log.d(tag, "logCursorMessageInfo SEEN: " +
                cursor.getString(cursor.getColumnIndex(Telephony.Sms.SEEN)));

        Log.d(tag, "logCursorMessageInfo STATUS: " +
                cursor.getString(cursor.getColumnIndex(Telephony.Sms.STATUS)));
    }
}
