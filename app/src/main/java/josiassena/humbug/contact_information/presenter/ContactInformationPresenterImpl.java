package josiassena.humbug.contact_information.presenter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.text.Html;
import android.text.Spanned;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import josiassena.humbug.contact_information.view.ContactInformationView;
import josiassena.humbug.helpers.HumBugSharedPreferences;
import josiassena.humbug.helpers.Utils;
import josiassena.humbug.msg.Conversation;

/**
 * File created by josiassena on 12/19/16.
 */
public class ContactInformationPresenterImpl extends MvpBasePresenter<ContactInformationView>
        implements ContactInformationPresenter {

    @Override
    public Spanned getActionBarTitle(final Conversation conversation) {
        if (conversation.getTitle() != null && !conversation.getTitle().trim().isEmpty()) {
            return getHtmlTitle(conversation.getTitle());
        } else {
            return getHtmlTitle(conversation.getPhoneNumber());
        }
    }

    @Override
    public void goToMoreContactInformationScreen(final Context context,
                                                 final Conversation conversation) {

        final Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI,
                Utils.getContactId(context, conversation.getPhoneNumber()));

        final Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(uri);

        context.startActivity(intent);
    }

    @NonNull
    private Spanned getHtmlTitle(final String title) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml("<font face='sans-serif-light' color='" +
                    HumBugSharedPreferences.getToolBarTextColor() + "'>" +
                    title + "</font>", Html.FROM_HTML_MODE_LEGACY, null, null);
        } else {
            return Html.fromHtml("<font face='sans-serif-light' color='" +
                    HumBugSharedPreferences.getToolBarTextColor() + "'>" +
                    title + "</font>");
        }
    }
}
