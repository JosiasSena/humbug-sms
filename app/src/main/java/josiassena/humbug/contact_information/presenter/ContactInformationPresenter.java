package josiassena.humbug.contact_information.presenter;

import android.content.Context;
import android.text.Spanned;

import com.hannesdorfmann.mosby.mvp.MvpPresenter;

import josiassena.humbug.contact_information.view.ContactInformationView;
import josiassena.humbug.msg.Conversation;

/**
 * File created by josiassena on 12/19/16.
 */
interface ContactInformationPresenter extends MvpPresenter<ContactInformationView> {

    Spanned getActionBarTitle(Conversation conversation);

    void goToMoreContactInformationScreen(Context context, final Conversation conversation);
}
