package josiassena.humbug.contact_information.view;

import android.Manifest;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.hannesdorfmann.mosby.mvp.MvpActivity;
import com.hannesdorfmann.mosby.mvp.MvpPresenter;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.listener.single.DialogOnDeniedPermissionListener;
import com.karumi.dexter.listener.single.PermissionListener;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import josiassena.humbug.R;
import josiassena.humbug.contact_information.presenter.ContactInformationPresenterImpl;
import josiassena.humbug.helpers.HumBugSharedPreferences;
import josiassena.humbug.msg.Conversation;

public class ContactInformationActivity
        extends MvpActivity<ContactInformationView, ContactInformationPresenterImpl> {

    private static final String TAG = ContactInformationActivity.class.getSimpleName();

    private Conversation conversation;

    @Bind (R.id.tv_person_ame)
    TextView tvPersonName;

    @Bind (R.id.tv_person_number)
    TextView tvPersonNumber;

    /**
     * Instantiate a presenter instance
     *
     * @return The {@link MvpPresenter} for this view
     */
    @NonNull
    @Override
    public ContactInformationPresenterImpl createPresenter() {
        return new ContactInformationPresenterImpl();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_information);
        ButterKnife.bind(this);
        getExtras();
    }

    private void getExtras() {
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            conversation = extras.getParcelable("conversation");
            if (conversation != null) {
                setupActionBar();

                setPersonNameAndPhoneNumberFromConversation();
            }
        } else {
            Log.e(TAG, "getExtras(): Extras are null.");
        }
    }

    private void setPersonNameAndPhoneNumberFromConversation() {
        tvPersonName.setText(josiassena.humbug.helpers.Utils
                .getContactName(this, conversation.getPhoneNumber()));

        tvPersonNumber.setText(conversation.getPhoneNumber());
    }

    private void setupActionBar() {
        final ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setBackgroundDrawable(
                    new ColorDrawable(HumBugSharedPreferences.getToolBarBackGroundColor()));
            actionBar.setTitle(presenter.getActionBarTitle(conversation));
        }
    }

    @OnClick (R.id.moreContactInfo)
    public void moreContactInfoClick() {
        presenter.goToMoreContactInformationScreen(this, conversation);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_contact_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        switch (itemId) {
            case R.id.action_call:
                PermissionListener dialogPermissionListener =
                        DialogOnDeniedPermissionListener.Builder
                                .withContext(this)
                                .withTitle(R.string.call_perm_dialog_title)
                                .withMessage(R.string.call_perm_dialog_body)
                                .withButtonText(android.R.string.ok)
                                .withIcon(R.drawable.icon)
                                .build();

                Dexter.checkPermission(dialogPermissionListener, Manifest.permission.CALL_PHONE);

                String phoneNumber = conversation.getPhoneNumber();
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + phoneNumber));
                startActivity(callIntent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
