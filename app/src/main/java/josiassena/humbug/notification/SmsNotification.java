package josiassena.humbug.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.telephony.SmsMessage;

import josiassena.humbug.R;
import josiassena.humbug.helpers.HumBugSharedPreferences;
import josiassena.humbug.helpers.Utils;
import josiassena.humbug.messages.view.TextMessagesActivity;

/**
 * File created by josias on 12/30/15.
 */
public class SmsNotification {

    private final SmsMessage smsMessage;
    private final int notificationCount;
    private Context context;

    public SmsNotification(Context context, SmsMessage smsMessage, int notificationCount) {
        this.context = context;
        this.smsMessage = smsMessage;
        this.notificationCount = notificationCount;
    }

    public SmsNotification display() {
        Intent intent = new Intent(context, TextMessagesActivity.class);
        intent.setAction(HumBugSharedPreferences.GO_TO_TEXT_MSG_ACTIVITY);
        intent.putExtra(HumBugSharedPreferences.COMING_FROM_NOTIFICATION, true);
        intent.putExtra(HumBugSharedPreferences.NOTIFICATION_PHONE_NUMBER,
                smsMessage.getOriginatingAddress());

        // This ensures that navigating backward from the Activity opened
        // from the notification leads back to the main/parent activity
        TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(context);
        taskStackBuilder.addParentStack(TextMessagesActivity.class);
        taskStackBuilder.addNextIntent(
                intent); // Adds the Intent that starts the Activity to the top of the stack

        PendingIntent pendingIntent = taskStackBuilder.getPendingIntent(
                HumBugSharedPreferences.NEW_MESSAGE_NOTIFICATION_REQUEST_CODE,
                PendingIntent.FLAG_ONE_SHOT);

        String messageBody = smsMessage.getDisplayMessageBody();
        String contactName = Utils.getContactName(context, smsMessage.getOriginatingAddress());
        if (contactName.trim().isEmpty()) {
            contactName = smsMessage.getOriginatingAddress();
        }

        NotificationCompat.Builder notificationCompat = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.icon)
                .setContentTitle(contactName)
                .setContentText(messageBody)
                .setContentIntent(pendingIntent)
                .setTicker(messageBody)
                .setOngoing(false)
                .setLights(Color.BLUE, 1000, 1000) // (color, ms it stays on, ms it stays off)
                .setSound(getNotificationSound())
                .setStyle(getNotificationStyle(messageBody))
                .setPriority(NotificationCompat.PRIORITY_MAX);

        if (Utils.getSharedPreferenceBoolean(context, "notifications_new_message_vibrate")) {
            notificationCompat.setVibrate(getNotificationVibratePattern());
        }

        if (Build.VERSION.SDK_INT >= 21) {
            notificationCompat.setVisibility(Notification.VISIBILITY_PUBLIC);
        }

        final NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(HumBugSharedPreferences.NEW_MESSAGE_NOTIFICATION_ID,
                notificationCompat.build());

        return this;
    }

    private NotificationCompat.Style getNotificationStyle(String messageBody) {
        return new NotificationCompat.BigTextStyle()
                .bigText(messageBody)
                .setSummaryText(context.getResources()
                        .getQuantityString(R.plurals.notification_count, notificationCount,
                                notificationCount));
    }

    private Uri getNotificationSound() {
        SharedPreferences defaultSharedPreferences = PreferenceManager.
                getDefaultSharedPreferences(context);

        String alarm = defaultSharedPreferences.getString("notifications_new_message_ringtone",
                RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION).toString());

        return Uri.parse(alarm);
    }

    private long[] getNotificationVibratePattern() {
        if (Utils.getSharedPreferenceString(context, "vibration_pattern").equals("Short")) {
            return new long[] {100, 300, 100, 300};
        } else if (Utils.getSharedPreferenceString(context, "vibration_pattern").equals("Medium")) {
            return new long[] {100, 300, 100, 300, 100, 300, 100, 300};
        } else if (Utils.getSharedPreferenceString(context, "vibration_pattern").equals("Long")) {
            return new long[] {100, 300, 100, 300, 100, 300, 100, 300, 100, 300, 100, 300};
        }

        return new long[] {100, 300, 100, 300};
    }
}