package josiassena.humbug.msg;

import android.net.Uri;

public class MessageConstants {

    private static final Uri MMS_CONTENT_URI = Uri.parse("content://mms");

    public static final Uri MMS_INBOX_CONTENT_URI = Uri.withAppendedPath(MMS_CONTENT_URI, "inbox");
    public static final Uri MMS_SENTBOX_CONTENT_URI = Uri.withAppendedPath(MMS_CONTENT_URI, "sent");
    public static final Uri MMS_PART_CONTENT_URI = Uri.withAppendedPath(MMS_CONTENT_URI, "part");

    public static final int MMS_TYPE_SENT = 128;
    public static final int MMS_TYPE_RECEIVED = 132;
    public static final int MMS_TYPE_ADDR_RECIPIENT = 151;
    public static final int MMS_TYPE_ADDR_SENDER = 137;
}