package josiassena.humbug.msg;

/**
 * File created by josiassena on 12/20/16.
 */
final class Part {

    private byte[] media;
    private String contentType;
    private String name;

    Part(byte[] media, String contentType, String name) {
        this.media = media;
        this.contentType = contentType;
        this.name = name;
    }

    public byte[] getMedia() {
        return media;
    }

    public String getContentType() {
        return contentType;
    }

    public String getName() {
        return name;
    }
}
