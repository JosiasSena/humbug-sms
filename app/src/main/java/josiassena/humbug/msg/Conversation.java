package josiassena.humbug.msg;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * File created by josias on 12/30/15.
 */
public class Conversation implements Parcelable {

    private String excerpt;
    private int id;
    private int messageCount;
    private String phoneNumber;
    private String time;
    private String title;
    private int unreadMessageCount;

    private Conversation(String excerpt, int id, int messageCount, String phoneNumber, String time,
                         String title, int unreadMessageCount) {
        this.excerpt = excerpt;
        this.id = id;
        this.messageCount = messageCount;
        this.phoneNumber = phoneNumber;
        this.time = time;
        this.title = title;
        this.unreadMessageCount = unreadMessageCount;
    }

    private Conversation(Parcel in) {
        excerpt = in.readString();
        id = in.readInt();
        messageCount = in.readInt();
        phoneNumber = in.readString();
        time = in.readString();
        title = in.readString();
        unreadMessageCount = in.readInt();
    }

    public static final Creator<Conversation> CREATOR = new Creator<Conversation>() {
        @Override
        public Conversation createFromParcel(Parcel in) {
            return new Conversation(in);
        }

        @Override
        public Conversation[] newArray(int size) {
            return new Conversation[size];
        }
    };

    /**
     * Describe the kinds of special objects contained in this Parcelable's marshalled
     * representation.
     *
     * @return a bitmask indicating the set of special object types marshalled by the Parcelable.
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Flatten this object in to a Parcel.
     *
     * @param dest  The Parcel in which the object should be written.
     * @param flags Additional flags about how the object should be written. May be 0 or {@link
     *              #PARCELABLE_WRITE_RETURN_VALUE}.
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(excerpt);
        dest.writeInt(id);
        dest.writeInt(messageCount);
        dest.writeString(phoneNumber);
        dest.writeString(time);
        dest.writeString(title);
        dest.writeInt(unreadMessageCount);
    }

    public static class Builder {

        private String excerpt;
        private int id;
        private int messageCount;
        private String phoneNumber;
        private String time;
        private String title;
        private int unreadMessageCount;

        public Builder setExcerpt(String excerpt) {
            this.excerpt = excerpt;
            return this;
        }

        public Builder setId(int id) {
            this.id = id;
            return this;
        }

        public Builder setMessageCount(int messageCount) {
            this.messageCount = messageCount;
            return this;
        }

        public Builder setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        public Builder setTime(String time) {
            this.time = time;
            return this;
        }

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setUnreadMessageCount(int unreadMessageCount) {
            this.unreadMessageCount = unreadMessageCount;
            return this;
        }

        public Conversation build() {
            return new Conversation(excerpt, id, messageCount, phoneNumber, time, title,
                    unreadMessageCount);
        }
    }

    public String getExcerpt() {
        return excerpt;
    }

    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMessageCount() {
        return messageCount;
    }

    public void setMessageCount(int messageCount) {
        this.messageCount = messageCount;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getUnreadMessageCount() {
        return unreadMessageCount;
    }

    public void setUnreadMessageCount(int unreadMessageCount) {
        this.unreadMessageCount = unreadMessageCount;
    }

    @Override
    public String toString() {
        return "Conversation{" +
                "excerpt='" + excerpt + '\'' +
                ", id=" + id +
                ", messageCount=" + messageCount +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", time='" + time + '\'' +
                ", title='" + title + '\'' +
                ", unreadMessageCount=" + unreadMessageCount +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Conversation that = (Conversation) o;
        return id == that.id && phoneNumber.equals(that.phoneNumber) && title.equals(that.title);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + phoneNumber.hashCode();
        result = 31 * result + title.hashCode();
        return result;
    }

}
