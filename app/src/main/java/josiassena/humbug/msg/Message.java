package josiassena.humbug.msg;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * File created by josias on 12/30/15.
 */
public class Message implements Parcelable {

    /**
     * The phone number the message is being sent to
     */
    private String phoneNumber;

    /**
     * The message being sent
     */
    private String message;

    /**
     * The time the message was being sent
     */
    private long timeOfText;

    /**
     * The messageType of message.
     */
    private MessageType messageType;

    /**
     * The name of the person the message is going to be sent to
     */
    private String name;

    /**
     * Is the message a fake message or not?
     */
    private boolean isFake;

    private boolean isMMS;
    private boolean save;

    private String[] addresses;
    private Bitmap[] images;
    private String[] imageNames;
    private List<Part> parts = new ArrayList<>();
    private int delay;

    //region Constructors

    /**
     * Default constructor
     */
    public Message() {
        this("", new String[] {""});
    }

    /**
     * Constructor
     *
     * @param message   is the message to send
     * @param addresses is an array of phone numbers to send to
     */
    private Message(String message, String[] addresses) {
        this.message = message;
        this.addresses = addresses;
        this.images = new Bitmap[0];
        this.save = true;
        this.messageType = MessageType.THEM;
        this.delay = 0;
    }

    private Message(final String phoneNumber, final String message, final String name,
                    final long timeOfText, final MessageType messageType, final int delay,
                    final boolean isFake,
                    final boolean isMMS, final boolean save, final String[] addresses,
                    final Bitmap[] images, final String[] imageNames, final List<Part> parts) {
        this.phoneNumber = phoneNumber;
        this.message = message;
        this.name = name;
        this.timeOfText = timeOfText;
        this.messageType = messageType;
        this.delay = delay;
        this.isFake = isFake;
        this.isMMS = isMMS;
        this.save = save;
        this.addresses = addresses;
        this.images = images;
        this.imageNames = imageNames;
        this.parts = parts;
    }
    //endregion

    //region Builder
    public static class Builder {

        private MessageType messageType;
        private String phoneNumber;
        private String message;
        private String name;

        private long timeOfText;
        private int delay;

        private boolean isFake;
        private boolean isMMS;
        private boolean save;

        private String[] addresses;
        private Bitmap[] images;
        private String[] imageNames;
        private List<Part> parts = new ArrayList<>();

        public Builder setPhoneNumber(final String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        public Builder setMessage(final String message) {
            this.message = message;
            return this;
        }

        public Builder setName(final String name) {
            this.name = name;
            return this;
        }

        public Builder setTimeOfText(final long timeOfText) {
            this.timeOfText = timeOfText;
            return this;
        }

        public Builder setMessageType(final MessageType messageType) {
            this.messageType = messageType;
            return this;
        }

        public Builder setDelay(final int delay) {
            this.delay = delay;
            return this;
        }

        public Builder setFake(final boolean fake) {
            isFake = fake;
            return this;
        }

        public Builder setMMS(final boolean MMS) {
            isMMS = MMS;
            return this;
        }

        public Builder setSave(final boolean save) {
            this.save = save;
            return this;
        }

        public Builder setAddresses(final String[] addresses) {
            this.addresses = addresses;
            return this;
        }

        public Builder setImages(final Bitmap[] images) {
            this.images = images;
            return this;
        }

        public Builder setImageNames(final String[] imageNames) {
            this.imageNames = imageNames;
            return this;
        }

        public Builder setParts(final List<Part> parts) {
            this.parts = parts;
            return this;
        }

        public Message build() {
            return new Message(phoneNumber, message, name, timeOfText, messageType, delay, isFake,
                    isMMS,
                    save, addresses, images, imageNames, parts);
        }

    }
    //endregion

    //region Getters and Setters

    /**
     * Sets recipients
     *
     * @param addresses is the array of recipients to send to
     */
    public void setAddresses(String[] addresses) {
        this.addresses = addresses;
    }

    /**
     * Sets single recipient
     *
     * @param address is the phone number of the recipient
     */
    public void setAddress(String address) {
        this.addresses = new String[1];
        this.addresses[0] = address;
    }

    /**
     * Sets images
     *
     * @param images is the array of images to send to recipient
     */
    public void setImages(Bitmap[] images) {
        this.images = images;
    }

    /**
     * Sets image names
     *
     * @param names
     */
    public void setImageNames(String[] names) {
        this.imageNames = names;
    }

    /**
     * Sets image
     *
     * @param image is the single image to send to recipient
     */
    public void setImage(Bitmap image) {
        this.images = new Bitmap[1];
        this.images[0] = image;
    }

    /**
     * Sets audio file.  Must be in wav format.
     *
     * @param audio is the single audio sample to send to recipient
     */
    @Deprecated
    public void setAudio(byte[] audio) {
        addAudio(audio);
    }

    /**
     * Sets audio file.  Must be in wav format.
     *
     * @param audio is the single audio sample to send to recipient
     */
    public void addAudio(byte[] audio) {
        addMedia(audio, "audio/wav");
    }

    /**
     * Sets video file
     *
     * @param video is the single video sample to send to recipient
     */
    @Deprecated
    public void setVideo(byte[] video) {
        addVideo(video);
    }

    /**
     * Adds video file
     *
     * @param video is the single video sample to send to recipient
     */
    public void addVideo(byte[] video) {
        addMedia(video, "video/3gpp");
    }

    /**
     * Sets other media
     *
     * @param media    is the media you want to send
     * @param mimeType is the mimeType of the media
     */
    @Deprecated
    public void setMedia(byte[] media, String mimeType) {
        addMedia(media, mimeType);
    }

    /**
     * Adds other media
     *
     * @param media    is the media you want to send
     * @param mimeType is the mimeType of the media
     */
    public void addMedia(byte[] media, String mimeType) {
        this.parts.add(new Part(media, mimeType, null));
    }

    /**
     * Sets whether or not to save a message to the database
     *
     * @param save is whether or not to save the message
     */
    public void setSave(boolean save) {
        this.save = save;
    }

    /**
     * Sets the time delay before sending a message NOTE: this is only applicable for SMS messages
     *
     * @param delay the time delay in milliseconds
     */
    public void setDelay(int delay) {
        this.delay = delay;
    }

    /**
     * Method to add another recipient to the object
     *
     * @param address is the string of the recipients phone number to add to end of recipients
     *                array
     */
    public void addAddress(String address) {
        String[] temp = this.addresses;

        if (temp == null) {
            temp = new String[0];
        }

        this.addresses = new String[temp.length + 1];

        System.arraycopy(temp, 0, this.addresses, 0, temp.length);

        this.addresses[temp.length] = address;
    }

    /**
     * Add another image to the object
     *
     * @param image is the image that you want to add to the end of the bitmaps array
     */
    public void addImage(Bitmap image) {
        Bitmap[] temp = this.images;

        if (temp == null) {
            temp = new Bitmap[0];
        }

        this.images = new Bitmap[temp.length + 1];

        for (int i = 0; i < temp.length; i++) {
            this.images[i] = temp[i];
        }

        this.images[temp.length] = image;
    }

    /**
     * Gets the addresses of the message
     *
     * @return an array of strings with all of the addresses
     */
    public String[] getAddresses() {
        return this.addresses;
    }

    /**
     * Gets the images in the message
     *
     * @return an array of bitmaps with all of the images
     */
    public Bitmap[] getImages() {
        return this.images;
    }

    /**
     * Gets image names for the message
     *
     * @return
     */
    public String[] getImageNames() {
        return this.imageNames;
    }

    /**
     * Gets the audio sample in the message
     *
     * @return an array of bytes with audio information for the message
     */
    public List<Part> getParts() {
        return this.parts;
    }

    /**
     * Gets whether or not to save the message to the database
     *
     * @return a boolean of whether or not to save
     */
    public boolean getSave() {
        return this.save;
    }

    /**
     * Gets the time to delay before sending the message
     *
     * @return the delay time in milliseconds
     */
    public int getDelay() {
        return this.delay;
    }

    /**
     * Static method to convert a bitmap into a byte array to easily send it over http
     *
     * @param image is the image to convert
     * @return a byte array of the image data
     */
    public static byte[] bitmapToByteArray(Bitmap image) {
        if (image == null) {
            Log.v("SimpleSms", "image is null, returning byte array of size 0");
            return new byte[0];
        }

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 90, stream);
        return stream.toByteArray();
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getTimeOfText() {
        return timeOfText;
    }

    public void setTimeOfText(long timeOfText) {
        this.timeOfText = timeOfText;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isFake() {
        return isFake;
    }

    public void setFake(boolean fake) {
        isFake = fake;
    }

    public boolean isMMS() {
        return isMMS;
    }

    public void setIsMms(boolean MMS) {
        isMMS = MMS;
    }
    //endregion

    //region Parcelable Implementation
    protected Message(Parcel in) {
        phoneNumber = in.readString();
        message = in.readString();
        timeOfText = in.readLong();
        name = in.readString();
        isFake = in.readByte() != 0;
        isMMS = in.readByte() != 0;
        save = in.readByte() != 0;
        addresses = in.createStringArray();
        images = in.createTypedArray(Bitmap.CREATOR);
        imageNames = in.createStringArray();
        delay = in.readInt();
    }

    public static final Creator<Message> CREATOR = new Creator<Message>() {
        @Override
        public Message createFromParcel(Parcel in) {
            return new Message(in);
        }

        @Override
        public Message[] newArray(int size) {
            return new Message[size];
        }
    };

    /**
     * Describe the kinds of special objects contained in this Parcelable instance's marshaled
     * representation. For example, if the object will include a file descriptor in the output of
     * {@link #writeToParcel(Parcel, int)}, the return value of this method must include the {@link
     * #CONTENTS_FILE_DESCRIPTOR} bit.
     *
     * @return a bitmask indicating the set of special object types marshaled by this Parcelable
     * object instance.
     * @see #CONTENTS_FILE_DESCRIPTOR
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Flatten this object in to a Parcel.
     *
     * @param dest  The Parcel in which the object should be written.
     * @param flags Additional flags about how the object should be written. May be 0 or {@link
     *              #PARCELABLE_WRITE_RETURN_VALUE}.
     */
    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeString(phoneNumber);
        dest.writeString(message);
        dest.writeLong(timeOfText);
        dest.writeString(name);
        dest.writeByte((byte) (isFake ? 1 : 0));
        dest.writeByte((byte) (isMMS ? 1 : 0));
        dest.writeByte((byte) (save ? 1 : 0));
        dest.writeStringArray(addresses);
        dest.writeTypedArray(images, flags);
        dest.writeStringArray(imageNames);
        dest.writeInt(delay);
    }
    //endregion
}
