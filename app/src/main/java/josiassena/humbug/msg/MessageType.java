package josiassena.humbug.msg;

import android.support.annotation.IntRange;

import java.io.InvalidObjectException;

public enum MessageType {

    THEM(1), US(2);

    private int value;

    public int getValue() {
        return value;
    }

    MessageType(int value) {
        this.value = value;
    }

    public static MessageType getMessageType(@IntRange (from = 1, to = 2) int value)
            throws InvalidObjectException {
        if (value == THEM.getValue()) {
            return MessageType.THEM;
        } else if (value == US.getValue()) {
            return MessageType.US;
        } else {
            throw new InvalidObjectException(
                    "The value: " + value + "is invalid. Value must be either 1 or 2.");
        }
    }
}