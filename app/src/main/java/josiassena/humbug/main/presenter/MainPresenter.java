package josiassena.humbug.main.presenter;

import android.content.Context;

import com.hannesdorfmann.mosby.mvp.MvpPresenter;

import josiassena.humbug.main.view.MainView;

/**
 * File created by josias on 12/30/15.
 */
interface MainPresenter extends MvpPresenter<MainView> {
    void getConversations(Context context);

    void getConversationToMarkUnread(Context context, String smsPhoneNumber);
}
