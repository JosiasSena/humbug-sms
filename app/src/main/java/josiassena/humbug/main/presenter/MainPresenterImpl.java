package josiassena.humbug.main.presenter;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.ArrayList;

import josiassena.humbug.helpers.Utils;
import josiassena.humbug.main.view.MainView;
import josiassena.humbug.msg.Conversation;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func0;
import rx.schedulers.Schedulers;

/**
 * File created by josias on 12/30/15.
 */
public class MainPresenterImpl extends MvpBasePresenter<MainView> implements MainPresenter {

    private static final String TAG = MainPresenterImpl.class.getSimpleName();

    @Override
    public void getConversations(Context context) {
        Log.d(TAG, "getConversations() was called");

        Observable.defer(getConversationListObservable(context))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ArrayList<Conversation>>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "getConversations.onCompleted() was called");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError() called with: " + "e = [" + e.getMessage() + "]", e);
                    }

                    @Override
                    public void onNext(ArrayList<Conversation> conversations) {
                        if (getView() != null && isViewAttached()) {
                            getView().onGotConversations(conversations);
                        }
                    }
                });

    }

    @Override
    public void getConversationToMarkUnread(Context context, String smsPhoneNumber) {
        Log.d(TAG, "getConversationToMarkUnread() was called");

        Observable.defer(getConversationObservable(context, smsPhoneNumber))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Conversation>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError() called with: " + "e = [" + e.getMessage() + "]", e);
                    }

                    @Override
                    public void onNext(Conversation conversation) {
                        if (getView() != null && isViewAttached()) {
                            if (conversation != null) {
                                getView().onGotConversationToMarkUnread(conversation);
                            }
                        }
                    }
                });
    }

    @NonNull
    private Func0<Observable<ArrayList<Conversation>>> getConversationListObservable(
            final Context context) {
        return new Func0<Observable<ArrayList<Conversation>>>() {
            @Override
            public Observable<ArrayList<Conversation>> call() {
                return Observable.just(getConversationsArray(context));
            }
        };
    }

    private ArrayList<Conversation> getConversationsArray(Context context) {
        Log.d(TAG, "getConversationsArray() was called");

        Uri uri = Uri.parse("content://mms-sms/conversations/?simple=true");
        Cursor cursor = context.getContentResolver()
                .query(uri, new String[] {"*"}, null, null, "date DESC");

        ArrayList<Conversation> conversations = new ArrayList<>();
        Utils.logCursorColumns(TAG, cursor);

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                int id = cursor.getInt(cursor.getColumnIndex("_id"));
                String recipient_ids = Utils.getContactPhoneNumber(context,
                        cursor.getInt(cursor.getColumnIndex("recipient_ids")));
                String contactName = Utils.getContactName(context, recipient_ids);

                int unreadCount;

                try {
                    unreadCount = cursor.getInt(cursor.getColumnIndexOrThrow("unread_count"));
                } catch (IllegalArgumentException e) {
                    Log.e(TAG, e.getMessage(), e);
                    unreadCount = 0;
                }

                conversations.add(new
                        Conversation.Builder()
                        .setId(id)
                        .setTitle(contactName)
                        .setPhoneNumber(recipient_ids)
                        .setUnreadMessageCount(unreadCount)
                        .setMessageCount(cursor.getInt(cursor.getColumnIndex("message_count")))
                        .setExcerpt(cursor.getString(cursor.getColumnIndex("snippet")))
                        .setTime(cursor.getString(cursor.getColumnIndex("date"))).build());

                cursor.moveToNext();
            }
            cursor.close();
        }

        return conversations;
    }

    @NonNull
    private Func0<Observable<Conversation>> getConversationObservable(
            final Context context, final String smsPhoneNumber) {
        return new Func0<Observable<Conversation>>() {
            @Override
            public Observable<Conversation> call() {
                return Observable.just(getConversationObject(context, smsPhoneNumber));
            }
        };
    }

    private Conversation getConversationObject(Context context, String smsPhoneNumber) {

        Conversation conversation = null;

        Uri uri = Uri.parse("content://mms-sms/conversations/?simple=true");

        Cursor cursor = context.getContentResolver()
                .query(uri, new String[] {"*"}, null, null, "date DESC");

        Utils.logCursorColumns(TAG, cursor);

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                String contactPhoneNumber = Utils.getContactPhoneNumber(context,
                        cursor.getInt(cursor.getColumnIndex("recipient_ids")));

                if (contactPhoneNumber.equals(smsPhoneNumber)) {
                    int id = cursor.getInt(cursor.getColumnIndex("_id"));

                    String contactName = Utils.getContactName(context, contactPhoneNumber);

                    int unreadCount;

                    try {
                        unreadCount = cursor.getInt(cursor.getColumnIndexOrThrow("unread_count"));
                    } catch (IllegalArgumentException e) {
                        Log.e(TAG, e.getMessage(), e);
                        unreadCount = 0;
                    }

                    conversation = new Conversation.Builder()
                            .setId(id)
                            .setTitle(contactName)
                            .setPhoneNumber(contactPhoneNumber)
                            .setUnreadMessageCount(unreadCount)
                            .setMessageCount(cursor.getInt(cursor.getColumnIndex("message_count")))
                            .setExcerpt(cursor.getString(cursor.getColumnIndex("snippet")))
                            .setTime(cursor.getString(cursor.getColumnIndex("date"))).build();
                }

                cursor.moveToNext();
            }

            cursor.close();
        }

        return conversation;
    }

}
