package josiassena.humbug.main.view;

import com.hannesdorfmann.mosby.mvp.MvpView;

import java.util.ArrayList;

import josiassena.humbug.msg.Conversation;

/**
 * File created by josias on 12/30/15.
 */
public interface MainView extends MvpView {
    void onNewConversationCreated(Conversation conversation);

    void onGotConversations(ArrayList<Conversation> conversations);

    void onGotConversationToMarkUnread(Conversation conversation);
}
