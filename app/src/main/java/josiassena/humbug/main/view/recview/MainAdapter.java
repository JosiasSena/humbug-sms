package josiassena.humbug.main.view.recview;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import josiassena.humbug.R;
import josiassena.humbug.helpers.HumBugSharedPreferences;
import josiassena.humbug.messages.view.TextMessagesActivity;
import josiassena.humbug.msg.Conversation;

/**
 * File created by josias on 12/30/15.
 */
public class MainAdapter extends RecyclerView.Adapter<ViewHolder> {

    private static final String TAG = MainAdapter.class.getSimpleName();

    private Activity activity;
    private List<Conversation> conversations;

    public MainAdapter(Activity activity, final ArrayList<Conversation> conversations) {
        this.activity = activity;
        this.conversations = conversations;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater.from(activity).inflate(R.layout.main_list_item, parent, false));
    }

    @SuppressLint ("DefaultLocale")
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final josiassena.humbug.msg.Conversation conversation = conversations.get(position);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            holder.smsInitial.getBackground()
                    .setTint(HumBugSharedPreferences.getToolBarBackGroundColor());
        } else {
            holder.smsInitial.getBackground()
                    .setColorFilter(HumBugSharedPreferences.getToolBarBackGroundColor(),
                            PorterDuff.Mode.MULTIPLY);
        }

        if (conversation.getUnreadMessageCount() > 0) {
            holder.smsTitle.setTypeface(null, Typeface.BOLD);
        } else {
            holder.smsTitle.setTypeface(null, Typeface.NORMAL);
        }

        if (conversation.getTitle().trim().isEmpty()) {
            holder.smsInitial.setText("#");
            holder.smsTitle.setText(String.format("%s (%d)", conversation.getPhoneNumber(),
                    conversation.getMessageCount()));
        } else {
            holder.smsInitial.setText(String.format("%s", conversation.getTitle().charAt(0)));
            holder.smsTitle.setText(String.format("%s (%d)", conversation.getTitle(),
                    conversation.getMessageCount()));
        }

        holder.smsExcerpt.setText(conversation.getExcerpt());
        holder.smsTimeStamp.setText(getDate(Long.parseLong(conversation.getTime())));
        holder.itemView.setOnClickListener(new ConversationOnclickListener(conversation));
        holder.itemView.setOnLongClickListener(new OnLongHoldClickListener(conversation));
    }

    public void markUnread(Conversation conversation) {
        if (conversations.contains(conversation)) {
            int index = conversations.indexOf(conversation);
            conversations.set(index, conversation);

            notifyItemChanged(conversations.indexOf(conversation));
        } else {
            conversations.add(0, conversation);
            notifyItemInserted(0);
        }
    }

    private static String getDate(long paramLong) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(paramLong);
        return new PrettyTime().format(calendar.getTime());
    }

    @Override
    public int getItemCount() {
        return null != conversations ? conversations.size() : 0;
    }

    private class ConversationOnclickListener implements View.OnClickListener {

        private Conversation conversation;

        ConversationOnclickListener(Conversation conversation) {
            this.conversation = conversation;
        }

        public void onClick(View view) {
            Bundle bundle = new Bundle();
            bundle.putParcelable("conversation", conversation);
            Intent intent = new Intent(activity, TextMessagesActivity.class);
            intent.putExtras(bundle);
            activity.startActivityForResult(intent, HumBugSharedPreferences.MARKING_CONVO_READ);
        }
    }

    private class OnLongHoldClickListener implements View.OnLongClickListener {

        private final Conversation conversation;

        OnLongHoldClickListener(Conversation conversation) {
            this.conversation = conversation;
        }

        /**
         * Called when a view has been clicked and held.
         *
         * @param v The view that was clicked and held.
         * @return true if the callback consumed the long click, false otherwise.
         */
        @Override
        public boolean onLongClick(View v) {
            showDeleteDialog(conversation);
            return true;
        }
    }

    private void showDeleteDialog(final Conversation conversation) {
        new AlertDialog.Builder(activity)
                .setTitle(activity.getString(R.string.delete_convo))
                .setMessage(activity.getString(R.string.are_you_sure_conversation))
                .setPositiveButton(activity.getString(R.string.delete),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ContentResolver contentResolver = activity.getContentResolver();

                                // TODO: 4/6/16 Conversations are not beind deleted properly in
                                // the home screen. Need to figure out why.
                                deleteSmsMessagesUsingPhoneNumber(contentResolver, conversation);
//                                deleteMmsMessagesUsingId(contentResolver, conversation);
                            }
                        })
                .setNegativeButton(activity.getString(R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
    }

    private void deleteSmsMessagesUsingPhoneNumber(ContentResolver contentResolver,
                                                   Conversation conversation) {

        String selection =
                Telephony.Sms.Conversations.ADDRESS + " like '%" + conversation.getPhoneNumber() +
                        "%'";
        Cursor cursor = contentResolver
                .query(Telephony.Sms.Conversations.CONTENT_URI, null, selection, null, null);

        if (cursor != null && cursor.moveToFirst()) {

            final int index = conversations.indexOf(conversation);

            if (index != -1) {
                contentResolver
                        .delete(Uri.parse("content://sms/conversations/" + conversation.getId()),
                                selection, null);

                if (!conversations.isEmpty()) {
                    conversations.remove(index);
                    notifyItemRemoved(index);
                }
            } else {
                Log.e(TAG, "deleteSmsMessagesUsingPhoneNumber: Cannot delete. Index was -1");
            }

            cursor.close();
        }
    }
}