package josiassena.humbug.main.view;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.provider.Telephony;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.hannesdorfmann.mosby.mvp.MvpActivity;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import josiassena.humbug.R;
import josiassena.humbug.helpers.HumBugSharedPreferences;
import josiassena.humbug.helpers.Utils;
import josiassena.humbug.helpers.broadcast_receivers.mms.IncomingMmsBroadCastReceiver;
import josiassena.humbug.helpers.broadcast_receivers.sms.IncomingSmsBroadCastReceiver;
import josiassena.humbug.helpers.broadcast_receivers.sms.SmsSentBroadcastReceiver;
import josiassena.humbug.helpers.callbacks.MessageDeletedListener;
import josiassena.humbug.helpers.callbacks.NewMessageReceivedListener;
import josiassena.humbug.helpers.callbacks.NewMessageSentListener;
import josiassena.humbug.main.presenter.MainPresenterImpl;
import josiassena.humbug.main.view.recview.MainAdapter;
import josiassena.humbug.messages.view.TextMessagesActivity;
import josiassena.humbug.messages.view.rec_view.TextMessagesAdapter;
import josiassena.humbug.msg.Conversation;
import josiassena.humbug.msg.Message;
import josiassena.humbug.settings.SettingsActivity;

public class MainActivity extends MvpActivity<MainView, MainPresenterImpl>
        implements MainView, NewMessageReceivedListener, NewMessageSentListener,
        MessageDeletedListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int SET_DEFAULT_APP = 716;

    @Bind (R.id.defaultAppTv)
    TextView defaultAppTv;

    @Bind (R.id.mainRecView)
    RecyclerView mainRecView;

    @Bind (R.id.mainToolBar)
    Toolbar mainToolBar;

    @Bind (R.id.createNewConversationFab)
    FloatingActionButton createNewConversationFab;

    private ArrayList<Conversation> conversations = new ArrayList<>();
    private MainAdapter mainAdapter;
    private String[] permissions = new String[] {
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.READ_SMS,
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        HumBugSharedPreferences.setContext(this);

        initToolbar();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Utils.dismissMessageNotification(this);

        checkIfWeAreDefaultApp();
        init();
    }

    private void init() {
        addToCallBacks();

        getExtras();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
            if (permissions[0].equals(Manifest.permission.READ_CONTACTS)) {
                new AlertDialog.Builder(this)
                        .setTitle(getString(R.string.permission_needed))
                        .setMessage(getString(R.string.contats_permission_needed))
                        .setCancelable(false)
                        .setPositiveButton(getString(R.string.word_ok),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        goToAppSettings();
                                    }
                                }).show();
            }
        } else {
            if (permissions[0].equals(Manifest.permission.READ_CONTACTS)) {
                initConversationsAdapter();
            }
        }
    }

    private void goToAppSettings() {
        final Uri uri = Uri.fromParts("package", getPackageName(), null);

        final Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, uri);
        startActivity(intent);
    }

    private void getExtras() {
        String phoneNumber = getIntent().getStringExtra(HumBugSharedPreferences.CONVO_TO_MARK_READ);

        if (phoneNumber != null && !phoneNumber.trim().isEmpty()) {
            for (Conversation conversation : conversations) {
                if (conversation.getPhoneNumber().equals(phoneNumber)) {
                    conversation.setUnreadMessageCount(0);

                    if (mainAdapter != null) {
                        mainAdapter.notifyDataSetChanged();
                    }
                }
            }
        }
    }

    private void addToCallBacks() {
        SmsSentBroadcastReceiver.addNewMessageSentCallBack(this);
        IncomingSmsBroadCastReceiver.addNewMessageCallBack(this);
        IncomingMmsBroadCastReceiver.addNewMessageCallBack(this);
        TextMessagesAdapter.addNewMessageDeletedCallback(this);
    }

    private void checkIfWeAreDefaultApp() {
        if (!Telephony.Sms.getDefaultSmsPackage(this).equals(getPackageName())) {
            // We are not default app
            defaultAppTv.setVisibility(View.VISIBLE);
        } else {
            // We are the default app
            defaultAppTv.setVisibility(View.GONE);
            setContentView(R.layout.activity_main);
            ButterKnife.bind(this);
            initToolbar();
            initRecView();
        }
    }

    private void initToolbar() {
        mainToolBar.setBackgroundColor(HumBugSharedPreferences.getToolBarBackGroundColor());
        mainToolBar.setTitle(Html.fromHtml(
                "<font face='sans-serif-light' color='" +
                        HumBugSharedPreferences.getToolBarTextColor() +
                        "'>" + Utils.getSharedAppName(this) + "</font>"));

        createNewConversationFab
                .setBackgroundColor(HumBugSharedPreferences.getToolBarBackGroundColor());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            /// lollipop or higher
            createNewConversationFab.getBackground()
                    .setTint(HumBugSharedPreferences.getToolBarBackGroundColor());
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        } else {
            createNewConversationFab.getBackground()
                    .setColorFilter(HumBugSharedPreferences.getToolBarBackGroundColor(),
                            PorterDuff.Mode.MULTIPLY);
        }

        setSupportActionBar(mainToolBar);
    }

    private void initRecView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);

        mainRecView.setLayoutManager(linearLayoutManager);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) !=
                PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) !=
                        PackageManager.PERMISSION_GRANTED) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissions, 929);
            }
        } else {
            initConversationsAdapter();
        }
    }

    private void initConversationsAdapter() {
        presenter.getConversations(this);

        mainAdapter = new MainAdapter(this, conversations);
        mainRecView.setItemViewCacheSize(300);
        mainRecView.setAdapter(mainAdapter);
    }

    @NonNull
    public MainPresenterImpl createPresenter() {
        return new MainPresenterImpl();
    }

    @OnClick (R.id.defaultAppTv)
    public void defaultAppTvOnClick() {
        Intent intent = new Intent(Telephony.Sms.Intents.ACTION_CHANGE_DEFAULT);
        intent.putExtra("package", getPackageName());
        startActivityForResult(intent, SET_DEFAULT_APP);
    }

    @OnClick (R.id.createNewConversationFab)
    public void onFabClick() {
        Intent intent = new Intent(MainActivity.this, TextMessagesActivity.class);
        intent.putExtra(HumBugSharedPreferences.STARTING_NEW_CONVERSATION, true);
        startActivityForResult(intent, HumBugSharedPreferences.START_NEW_CONVERSATION);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult() called with: " + "requestCode = [" + requestCode +
                "], resultCode = [" + resultCode + "], data = [" + data + "]");

        if (resultCode == MainActivity.RESULT_OK && requestCode == 1524) {
            onNewConversationCreated(
                    (Conversation) data.getParcelableExtra(HumBugSharedPreferences.FAKE_SMS));
        } else if (resultCode == MainActivity.RESULT_OK && requestCode == 716) {
            checkIfWeAreDefaultApp();
        } else if (resultCode == MainActivity.RESULT_CANCELED && requestCode == 716) {
            checkIfWeAreDefaultApp();
        } else if (resultCode == MainActivity.RESULT_OK &&
                requestCode == HumBugSharedPreferences.MARKING_CONVO_READ) {
            String phoneNumber = data.getStringExtra(HumBugSharedPreferences.CONVO_TO_MARK_READ);
            for (Conversation conversation : conversations) {
                if (conversation.getPhoneNumber().equals(phoneNumber)) {
                    conversation.setUnreadMessageCount(0);
                    mainAdapter.notifyDataSetChanged();
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        } else if (id == R.id.action_privacy_policy) {
            // TODO: 12/19/16
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onNewMessagesReceived(ArrayList<Message> messages) {
        Log.d("MainActivity", "onNewMessagesReceived() called with: messages = [" + messages + "]");

        for (Message message : messages) {
            presenter.getConversationToMarkUnread(this, message.getPhoneNumber());
        }
    }

    @Override
    public void onGotConversationToMarkUnread(Conversation conversation) {
        Log.d(TAG, "onGotConversationToMarkUnread() called with: " +
                "conversation = [" + conversation + "]");

        mainAdapter.markUnread(conversation);
    }

    @Override
    public void onNewMessageSent() {
        Log.d(TAG, "onNewMessageSent()");
    }

    public void onNewConversationCreated(Conversation conversation) {
        conversations.add(0, conversation);
        mainAdapter.notifyItemInserted(0);
    }

    @Override
    public void onGotConversations(ArrayList<Conversation> conversationArrayList) {

        boolean isAddedNewItem = false;

        for (Conversation conversation : conversationArrayList) {
            if (!conversations.contains(conversation)) {
                conversations.add(0, conversation);
                isAddedNewItem = true;
            }
        }

        if (isAddedNewItem) {
            mainAdapter.notifyItemInserted(0);
        }
    }

    @Override
    public void onNewMessageDeletedListener() {
        Log.e(TAG, "onNewMessageDeletedListener() called.");
        initRecView();
    }
}
