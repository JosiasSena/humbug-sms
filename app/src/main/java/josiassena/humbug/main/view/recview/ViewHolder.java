package josiassena.humbug.main.view.recview;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import josiassena.humbug.R;

/**
 * File created by josias on 12/30/15.
 */
public class ViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.smsExcerpt)
    public TextView smsExcerpt;

    @Bind(R.id.smsInitial)
    public TextView smsInitial;

    @Bind(R.id.smsTimeStamp)
    public TextView smsTimeStamp;

    @Bind(R.id.smsTitle)
    public TextView smsTitle;

    public ViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}